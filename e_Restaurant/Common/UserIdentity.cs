﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;

namespace e_Restaurant.Common
{
    [Serializable]
    public class UserIdentity : FormsIdentity
    {
        public long UserID { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string FullUserName { get; private set; }
        public string Email { get; set; }
        public string[] RoleName { get; private set; }

        public UserIdentity(FormsAuthenticationTicket ticket)
            : base(ticket)
        {
            if ((ticket.UserData != null) && (ticket.UserData.IndexOf("|") != -1))
            {
                string[] dataSections = ticket.UserData.Split('|');
                UserID = Convert.ToInt64(dataSections[0]);
                FirstName = dataSections[1];
                LastName = dataSections[2];
                FullUserName = dataSections[3];
                Email = dataSections[4];
                this.RoleName = Regex.Split(dataSections[5], ";");
            }
        }
    }
    public class UserPrincipal : GenericPrincipal
    {
        public UserPrincipal(UserIdentity identity)
            : base(identity, identity.RoleName)
        {
        }
    }
}