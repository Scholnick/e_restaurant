﻿using e_Restaurant.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;


namespace e_Restaurant.Common
{
    public class COM
    {
        private static string SmtpServer = "smtp.ukr.net";
        private static int SmtpPort = 465;
        public static string TranslateFileName = "~/App_Data/Resources.xml";
        private local db = new local();

        public class BrowserData
        {
            public static string Language
            {
                get
                {
                    return GetLanguageFromRequest(HttpContext.Current.Request);
                }
            }

            public static NameValueCollection QueryString
            {
                get
                {
                    return HttpContext.Current.Request.QueryString;
                }
            }
        }

        public class UserData
        {
            public static long ID
            {
                get
                {
                    return (HttpContext.Current.User.Identity as UserIdentity).UserID;

                }
            }

            public static string FirstName
            {
                get
                {
                    return (HttpContext.Current.User.Identity as UserIdentity).FirstName;

                }
            }

            public static string LastName
            {
                get
                {
                    return (HttpContext.Current.User.Identity as UserIdentity).LastName;

                }
            }

            public static string FullName
            {
                get
                {
                    return (HttpContext.Current.User.Identity as UserIdentity).FullUserName;

                }
            }

            public static string Email
            {
                get
                {
                    return (HttpContext.Current.User.Identity as UserIdentity).Email;

                }
            }

            public static string[] Role
            {
                get
                {
                    return (HttpContext.Current.User.Identity as UserIdentity).RoleName;

                }
            }
        }

        public class Url
        {
            public static string SiteUrl
            {
                get
                {
                    string absoluteUri = HttpContext.Current.Request.Url.AbsoluteUri;
                    return absoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "");

                    /*string.Format("{0}://{1}{2}{3}",
                    System.Web.HttpContext.Current.Request.Url.Scheme,
                    System.Web.HttpContext.Current.Request.Url.Host,
                    System.Web.HttpContext.Current.Request.Url.Port == 80 ? string.Empty : ":" + System.Web.HttpContext.Current.Request.Url.Port,
                    System.Web.HttpContext.Current.Request.ApplicationPath);*/
                }
            }

            public static string QueryString
            {
                get
                {
                    return HttpContext.Current.Request.Url.ToString();
                }
            }
        }


        private static string GetLanguageFromRequest(HttpRequest req)
        {
            string language = req.UserLanguages[0].ToLower();
            if (language.Length == 2 && language.IndexOf("-") == -1)
                return language;
            else
                return language.Split('-')[0];
        }

        public static void SendEmail(string RecipientAddress, string Subject, string Message)
        {
            string email = "scholnick2012@gmail.com";
            string password = "cneltyn2012";

            var loginInfo = new NetworkCredential(email, password);
            var msg = new MailMessage()
            {
                Subject = Subject,
                Body = Message,
                IsBodyHtml = true
            };

            msg.From = new MailAddress(email);
            msg.To.Add(new MailAddress(RecipientAddress));

            using (var smtpClient = new SmtpClient("smtp.gmail.com", 587))
            {
                using (var db = new local())
                {
                    var email_log = new t_email_log()
                    {
                        email_sender = email,
                        email_recipient = RecipientAddress,
                        email_body = msg.Body,
                        email_date_sent = DateTime.Now,
                        email_subject=Subject,
                        error_sending = false
                    };

                    try
                    {
                        smtpClient.EnableSsl = true;
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = loginInfo;
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtpClient.Send(msg);
                        db.t_email_log.Add(email_log);
                        db.SaveChanges();
                    }
                    catch (SmtpException ex)
                    {
                        email_log.error_sending = true;
                        email_log.email_subject += ". Fail: " + ex.Message;
                        db.t_email_log.Add(email_log);
                        db.SaveChanges();
                    }
                }
            }
        }


        public void CreateFilesTranslation()
        {
            var fileTranslation = new FileInfo(HttpContext.Current.Server.MapPath(TranslateFileName));
            if (fileTranslation.Exists && (DateTime.Now - fileTranslation.LastWriteTime).TotalDays >= 1)
            {
                fileTranslation.Delete();
                CreateXmlFile();
            }
            else if (!fileTranslation.Exists)
                CreateXmlFile();
        }

        private void CreateXmlFile()
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(docNode);

            var root = xmlDoc.AppendChild(xmlDoc.CreateElement("translate"));
            List<string> dictionaryList = db.t_dictionary.Select(d => d.dict_code).ToList();

            foreach (string dictionaryCode in dictionaryList)
            {
                var dictionaryTranslation = (from d in db.t_dictionary
                                             join ld in db.t_labels_dict on d.dict_id equals ld.dict_id into ldg
                                             from lab_dict in ldg.DefaultIfEmpty()
                                             join l in db.t_language on lab_dict.lan_id equals l.lan_id
                                             where d.dict_code == dictionaryCode
                                             orderby l.lan_id
                                             select new
                                             {
                                                 culture = lab_dict != null ? lab_dict.t_language.lan_google_code.ToLower() : l.lan_google_code.ToLower(),
                                                 translate = lab_dict.label_text

                                             }).ToList();

                var dictNode = xmlDoc.CreateElement("dictionary");
                dictNode.SetAttribute("dict_code", dictionaryCode);
                var dictionaryRoot = root.AppendChild(dictNode);

                foreach (var item in dictionaryTranslation)
                {
                    var tag = xmlDoc.CreateElement(item.culture);
                    tag.InnerText = item.translate;
                    dictionaryRoot.AppendChild(tag);
                }
            }

            string fullPath = HttpContext.Current.Server.MapPath(TranslateFileName);
            xmlDoc.Save(fullPath);
        }

        public static string GetTranslation(string dictionaryCode)
        {
            string result = "**" + dictionaryCode + "**";

            string brouserLanguage = BrowserData.Language;
            string fileTranslation = HttpContext.Current.Server.MapPath(TranslateFileName);
            if (File.Exists(fileTranslation))
            {
                var xmlDoc = XDocument.Load(fileTranslation);
                var translate = from l in xmlDoc.Descendants("dictionary").Where(r => (string)r.Attribute("dict_code") == dictionaryCode)
                                select l;//.Element(brouserLanguage).Value;

                return translate.FirstOrDefault() != null && !string.IsNullOrEmpty(translate.FirstOrDefault().Value)
                                    ? translate.FirstOrDefault().Element(brouserLanguage) != null
                                            ? translate.FirstOrDefault().Element(brouserLanguage).Value
                                            : ((XElement)translate.FirstOrDefault().FirstNode).Value
                                    : result;
            }
            else
                return result.Replace("_", " ").Replace("**", "");
        }

        public static string GetGoogleTranslate(string sourceText, string fromLanguage, string toLanguage)
        {
            string translatedTextJson = "";
            string url = String.Format("https://www.googleapis.com/language/translate/v2?key={0}&source={1}&target={2}&callback=translateText&q={3}",
                  "AIzaSyBW8Souh4T-i7B4VaZ_W7YvInqkW5pdl_s", fromLanguage, toLanguage, sourceText);
            using (WebClient webClient = new WebClient())
            {
                webClient.Encoding = System.Text.Encoding.UTF8;
                translatedTextJson = webClient.DownloadString(url);
                translatedTextJson = translatedTextJson.Replace("// API callback\ntranslateText(", "");
                translatedTextJson = translatedTextJson.Replace(");", "");
            }
            JavaScriptSerializer js = new JavaScriptSerializer();

            var translateResult = JsonConvert.DeserializeObject<TranslateResult>(translatedTextJson);
            return translateResult.data.translations[0].translatedText;
        }
    }
}