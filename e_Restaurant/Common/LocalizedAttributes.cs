﻿using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace e_Restaurant.Common
{
    public class LocalizedAttributes
    {
    }

    public class LocalizedDisplayName : DisplayNameAttribute
    {
        public LocalizedDisplayName(string displayNameKey)
            : base(displayNameKey)
        {

        }

        public override string DisplayName
        {
            get
            {
                return COM.GetTranslation(base.DisplayName);
            }
        }
    }

    public class RequiredLocalizedAttribute : RequiredAttribute
    {
        public string _displayName;

        public RequiredLocalizedAttribute()
        {
            this.ErrorMessageResourceName = "COMMON_REQUIRED_FIELD";
            this.ErrorMessage = "COMMON_REQUIRED_FIELD";
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            _displayName = validationContext.DisplayName;
            return base.IsValid(value, validationContext);
        }

        public override string FormatErrorMessage(string name)
        {
            return COM.GetTranslation(this.ErrorMessageResourceName);
        }
    }

    public class RegularExpressionLocalizedAttribute : RegularExpressionAttribute
    {

        public RegularExpressionLocalizedAttribute(string _pattern, string errorMessage)
            : base(_pattern)
        {
            this.ErrorMessage = errorMessage;
        }

        public override string FormatErrorMessage(string fieldName)
        {
            return COM.GetTranslation(this.ErrorMessage);
        }
    }

    public class StringLengthLocalizedAttribute : StringLengthAttribute
    {
        private string _displayName;
        public StringLengthLocalizedAttribute(int maximumLength, string errorMessage, int minimumLength)
            : base(maximumLength)
        {
            this.ErrorMessage = errorMessage;
            this.MinimumLength = minimumLength;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            _displayName = validationContext.DisplayName;
            return base.IsValid(value, validationContext);
        }

        public override string FormatErrorMessage(string fieldName)
        {
            return string.Format(COM.GetTranslation(this.ErrorMessage), _displayName, this.MinimumLength);
        }
    }

    public class CompareLocalizedAttribute : System.Web.Mvc.CompareAttribute,  IClientValidatable
    {
        public CompareLocalizedAttribute(string otherProperty, string errorMesage)
            : base(otherProperty)
        {
            this.ErrorMessage = errorMesage;
        }

        public override string FormatErrorMessage(string fieldName)
        {
            return COM.GetTranslation(this.ErrorMessage);
        }
    }
}