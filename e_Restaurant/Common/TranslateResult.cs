﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Common
{
    public class TranslateResult
    {
        public TranslateData data { get; set; }
    }
    public class TranslateData
    {
        public Translations[] translations { get; set; }
    }
    public class Translations
    {
        public string translatedText { get; set; }
    }
}