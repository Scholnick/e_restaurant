﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_Restaurant.Common
{
    public class EmailTemplate
    {
        public static string Registration =
            "<style type=\"text/css\">" +
            "    .footer {" +
            "        color: #ffffff;" +
            "        text-decoration: none;" +
            "        font-size: 12px;" +
            "        font-family: Arial,Helvetica,sans-serif;" +
            "    }" +
            "    .label {" +
            "        height: 32px;" +
            "        width: 124px;" +
            "        align: center;" +
            "        valign: middle;" +
            "    }" +
            "</style>" +
            "<div style=\"padding: 15px; background-color:#e9eaec; min-height:600px;color:#000;font-family:Playfair Display, Arial,Helvetica,sans-serif;font-size:12px\">" +
            "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#FFFFFF\" width=\"620\" align=\"center\" style=\"margin-top: 0; margin-bottom: 10px; color: #313131; font-size: 12px; font-family: Playfair Display, Arial,Helvetica,sans-serif\">" +
            "    <tr>" +
             "       <td valign=\"top\" bgcolor=\"#ffffff\">" +
             "            <a href=" + COM.Url.SiteUrl + " style=\"color: #ffffff\" target=\"_blank\">" +
             "                <img src=" + COM.Url.SiteUrl.TrimEnd('/') + UrlHelper.GenerateContentUrl("~/Images/photo.jpg", new HttpContextWrapper(HttpContext.Current)) + " alt=\"\" width=\"60\" style=\"margin: 18px 0px 0px 100px;\">" +
             "                <img class=\"logoimage\" src=" + COM.Url.SiteUrl.TrimEnd('/') + UrlHelper.GenerateContentUrl("~/Images/logo.jpg", new HttpContextWrapper(HttpContext.Current)) + " width=\"250\" style=\"display: block; margin: -60px 0px 0px 160px;\" alt=\"Logo\">" +
             "            </a>" +
             "            <p style=\"text-align: center; font-size: 16px; margin: 0px 0px 7px 20px;\">Your personal guide in the world of food </p>" +
             "            <img id=\"headline\" src=" + COM.Url.SiteUrl.TrimEnd('/') + UrlHelper.GenerateContentUrl("~/Images/EmailTemplate/line.PNG", new HttpContextWrapper(HttpContext.Current)) + " width=\"620\" height=\"3\" alt=\"line\">" +
             "        </td>" +
             "    </tr>" +
             "<tr>" +
             "    <td width=\"620\" height=\"20\"></td>" +
             "</tr>" +
             "<tr>" +
             "    <td valign=\"top\">" +
             "        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#ffffff\" width=\"584\" align=\"center\">" +
             "            <tr>" +
             "                <td height=\"40\" valign=\"top\" style=\"color: #313131; font-size: 12px; font-family: Arial,Helvetica,sans-serif\">Dear $User_name$," +
             "                </td>" +
             "            </tr>" +
             "            <tr>" +
             "                <td valign=\"top\" bgcolor=\"#FFFFFF\" style=\"color: #313131; font-size: 12px; font-family: Arial,Helvetica,sans-serif\">Your account  has been successfully created. Thank you for joining us!<br>" +
             "                    <br>" +
             "                    <span style=\"line-height: 19px;\">Your login: $Email$</span>" +
             "                    <br>" +
             "                    Your password: $password$" +
             "                            <br>" +
             "                    <br>" +
             "                    To enjoy all the benefits of our site, you just need to confirm your email by clicking this big red button." +
             "                </td>" +
             "            </tr>" +
             "            <tr>" +
             "                <td align=\"center\" valign=\"top\">" +
             "                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#ffffff\" width=\"584\" align=\"center\">" +
             "                        <tr>" +
             "                            <td width=\"584\" height=\"20\"></td>" +
             "                        </tr>" +
             "                    </table>" +
             "                </td>" +
             "            </tr>" +
             "            <tr>" +
             "                <td valign=\"top\" width=\"584\" height=\"30\"></td>" +
             "            </tr>" +
             "            <tr>" +
             "                <td valign=\"middle\" style=\"color: #313131; font-weight: bold; font-size: 12px; font-family: Arial,Helvetica,sans-serif\">" +
             "                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#ffffff\" width=\"584\" align=\"center\">" +
             "                        <tr>" +
             "                            <td align=\"left\">" +
             "                                <table height=\"31\" width=\"290\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#ffffff\" align=\"center\">" +
             "                                    <tr>" +
             "                                        <td width=\"290\" height=\"29\" align=\"center\" style=\"margin: auto;\">" +
             "                                            <a href=\"$URL$\" style=\"text-decoration: none;\"><span style=\"color: white; background: #992222; padding: 15px 15px; font-size: 14px; font-family: Playfair Display, Arial,Helvetica,sans-serif; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;\">CONFIRM EMAIL ADDRESS</span></a></td>" +
             "                                    </tr>" +
             "                                </table>" +
             "                            </td>" +
             "                        </tr>" +
             "                    </table>" +
             "                </td>" +
             "            </tr>" +
             "            <tr>" +
             "                <td valign=\"top\" width=\"584\" height=\"15\"></td>" +
             "            </tr>" +
             "        </table>" +
             "   <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#ffffff\" width=\"584\" align=\"center\">" +
             "               <tr>" +
             "                   <td colspan=\"2\" width=\"584\" height=\"30\"></td>" +
             "               </tr>" +
             "               <tr>" +
             "                   <td style=\"font-family: Arial,Helvetica,sans-serif; font-size: 12px; color: #313131\"></td>" +
             "               </tr>" +
             "               <tr>" +
             "                   <td height=\"62\" valign=\"middle\" style=\"background-color: rgb(16,40,76);\">" +
             "                       <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"620\" align=\"center\" style=\"background-color: rgb(16,40,76); margin-top: 0; color: #ffffff; font-size: 11px; font-family: Arial,Helvetica,sans-serif\">" +
             "                           <tr>" +
             "                               <td width=\"10\">&nbsp;</td>" +
             "                               <td width=\"10\">&nbsp;</td>" +
             "                               <td height=\"32\" width=\"124\" align=\"center\" valign=\"middle\">" +
             "                                   <a href=" + COM.Url.SiteUrl + " class=\"footer\">About us</a>" +
             "                               </td>" +
             "                               <td height=\"32\" width=\"124\" align=\"center\" valign=\"middle\">" +
             "                                   <a href=" + COM.Url.SiteUrl + " class=\"footer\">How it works</a>" +
             "                               </td>" +
             "                               <td height=\"32\" width=\"124\" align=\"center\" valign=\"middle\">" +
             "                                   <a href=" + COM.Url.SiteUrl + " class=\"footer\">Press about us</a>" +
             "                               </td>" +
             "                               <td height=\"32\" width=\"124\" align=\"center\" valign=\"middle\">" +
             "                                   <a href=" + COM.Url.SiteUrl + " class=\"footer\">Privacy Policy</a>" +
             "                               </td>" +
             "                               <td height=\"32\" width=\"124\" align=\"center\" valign=\"middle\">" +
             "                                   <a href=" + COM.Url.SiteUrl + " class=\"footer\">Contact us</a>" +
             "                               </td>" +
             "                               <tr>" +
             "                                   <td colspan=\"9\" align=\"center\" style=\"padding-top: 10px; font-family: Arial,Helvetica,sans-serif; font-size: 11px; color: #707070\">&copy; the E-restaurantNFC Copyright. All rights reserved" +
             "                                   </td>" +
             "                               </tr>" +
             "                           </tr>" +
             "                       </table>" +
             "                   </td>" +
             "               </tr>" +
             "           </table>" +
             "          </td>" +
             "      </tr>" +
             "  </table>";

    }
}