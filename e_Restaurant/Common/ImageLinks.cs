﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace e_Restaurant.Common
{
    public class ImageLink
    {
        private const string DEFAULT = "Images/Nofoto.png";
        private const string RESTAURANTDFAULT = "Images/UnitNofoto.png";
        private const string LOGOPATH = "Images/Restaurant/{0}/{1}";

        public class Restaurant
        {
            public static string Logo(string reference, string image, bool returnEmpty = false)
            {
                return string.IsNullOrEmpty(image)
                    ? (returnEmpty ? string.Empty : DEFAULT)
                        : string.Format(LOGOPATH, reference, image);
            }
        }

        public class Unit
        {
            public static string Image(string reference, string image)
            {
                return string.IsNullOrEmpty(image)
                    ? DEFAULT
                    : string.Format(LOGOPATH, reference, image); ;
            }
        }

        public class Article
        {
            public const string LogoDirectory = "/Images/Article/{0}/{1}";

            public const string UploadDirectory = "~/Images/Article/";
            public const string Directory = "~/Images/Article/{0}/";

            public static string FullPathLogo(int id, string image)
            {
                return string.IsNullOrEmpty(image)
                    ? DEFAULT
                    : string.Format(LogoDirectory, id, image);
            }
        }

        public class Types
        {
            private const string IconDirectory = "Images/Restaurant/{0}/icon/{1}";
            public static string FullIconPath(string restaurantReference, string iconName)
            {
                return string.IsNullOrEmpty(iconName)
                              ? string.Empty
                              : string.Format(IconDirectory, restaurantReference, iconName);
            }
        }
    }
}