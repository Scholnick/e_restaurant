﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Common
{
    public class ImageLink
    {
        private const string LOGOPATH = "../Images/Restaurant/{0}/{1}";
        public class Restaurant
        {
            public static string Logo(string reference, string image)
            {
                return string.IsNullOrEmpty(image)
                        ? string.Empty
                        : string.Format(LOGOPATH, reference, image);
            }
        }
    }
}