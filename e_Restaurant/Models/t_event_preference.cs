//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace e_Restaurant.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class t_event_preference
    {
        public t_event_preference()
        {
            this.t_user = new HashSet<t_user>();
        }
    
        public int eventpr_id { get; set; }
        public string eventpr_dict_code { get; set; }
    
        public virtual ICollection<t_user> t_user { get; set; }
    }
}
