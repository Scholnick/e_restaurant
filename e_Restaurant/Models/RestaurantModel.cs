﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Restaurant.Common;
using PagedList;
using PagedList.Mvc;
using System.ComponentModel.DataAnnotations;

namespace e_Restaurant.Models
{
    public class RestaurantModel
    {
    }

    public class RestaurantDetailsModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string AddressAditional { get; set; }
        public string Logo { get; set; }
        public string AditionalImage { get; set; }
        public string WorkingHours { get; set; }
        public string Langtitude { get; set; }
        public string LongTitude { get; set; }
        public string[] Payment { get; set; }
        public List<TypeModel> TypeList { get; set; }
        public IEnumerable<UnitModel> UnitList { get; set; }
        public Dictionary<string, string> SocialNetwork { get; set; }
        public List<RatingAverageModel> Rating { get; set; }        
        
        public string AverageRating { get; set; }

        [Required(ErrorMessage = "*")]
        [LocalizedDisplayName("LABEL_RATE_CATEGORY_FOOD")]
        [Range(1, 10, ErrorMessage = "*")]
        public int RateFood { get; set; }

        [Required(ErrorMessage = "*")]
        [LocalizedDisplayName("LABEL_RATE_CATEGORY_SERVICE")]
        [Range(1, 10, ErrorMessage = "*")]
        public int RateService { get; set; }

        [Required(ErrorMessage = "*")]
        [LocalizedDisplayName("LABEL_RATE_CATEGORY_AMBIENCE")]
        [Range(1, 10, ErrorMessage = "*")]
        public int RateAmbience { get; set; }

        [Required(ErrorMessage = "*")]
        [LocalizedDisplayName("LABEL_RATE_CATEGORY_VALUE")]
        [Range(1, 10, ErrorMessage = "*")]
        public int RateValue { get; set; }
    }

    public class RestaurantStartPageModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Ref { get; set; }
        public string Logo { get; set; }
        public string FullPathLogo { get; set; }
        public string Adress { get; set; }
        public string ZipCode { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string City { get; set; }
        public string Langtitude { get; set; }
        public string LongTitude { get; set; }
        public ArticleDetailsModel ArticleDetailsModel { get; set; }
        public int? CityID { get; set; }
    }

    public class RestaurantSearchCityModel
    {
        public IPagedList<RestaurantStartPageModel> Result { get; set; }
        public IEnumerable<ArticleDetailsModel> ArticlePopular { get; set; }
        public SearchFormModel SearchForm { get; set; }
        public RestaurantSearchCityModel()
        {
            SearchForm = new SearchFormModel();
        }
    }

}