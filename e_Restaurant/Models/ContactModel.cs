﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public class ContactModel
    {
        [RequiredLocalized]
        //[Required(ErrorMessage = "Введіть ім'я.")]
        //[StringLength(50, MinimumLength = 5, ErrorMessage = "Довжина повинна бути від 5 до 50 символів.")]
        [Display(Name = "You first name")]
        public string FirstName { get; set; }

        [RequiredLocalized]
        //[Required(ErrorMessage = "Введіть ім'я.")]
        //[StringLength(50, MinimumLength = 5, ErrorMessage = "Довжина повинна бути від 5 до 50 символів.")]
        [Display(Name = "You last name")]
        public string LastName { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_EMAIL_ADDRESS")]
        [RegularExpressionLocalizedAttribute(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", "COMMON_WRONG_EMAIL")]
        public string Email { get; set; }

        [RequiredLocalized]
        //[LocalizedDisplayName("")]
        [Display(Name = "Phone number")]
        public string Phone { get; set; }

        [RequiredLocalized]
        //[Required(ErrorMessage = "Тема повідомлення обов'язковa.")]
        [StringLength(150, MinimumLength = 5, ErrorMessage = "Довжина повинна бути від 5 до 150 символів.")]
        [Display(Name = "Issue of request")]
        public string Subject { get; set; }

        [RequiredLocalized]
        //[Required(ErrorMessage = "Введіть текст повідомлення.")]
        [Display(Name = "Your comment")]
        public string Message { get; set; }
    }
}