﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public class StartPageModel
    {
        public IEnumerable<ArticleDetailsModel> ArticleDet { get; set; }
        public IPagedList <RestaurantStartPageModel> RestaurantList { get; set; }
    }
}