﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public class UnitModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string NativeName { get; set; }
        public string Description { get; set; }
        public string NativeDescription { get; set; }
        public string Price { get; set; }
        public string Image { get; set; }
    }
}