﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using e_Restaurant.Common;
using e_Restaurant.Repositories;

using System.Web.Security;
using System.Globalization;
using PagedList;

namespace e_Restaurant.Models
{
    public class ArticleModel
    {
        public int ID { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("LABEL_ARTICLE_TITLE")]
        [StringLengthLocalized(55, "LABEL_VALIDATION_FIELD_ARTICLE", 6)]
        public string Title { get; set; }

        [LocalizedDisplayName("LABEL_ARTICLE_LOGO")]
        public string Logo { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("LABEL_ARTICLE_SHORT_TEXT")]
        [StringLengthLocalized(300, "LABEL_VALIDATION_FIELD_ARTICLE", 10)]
        public string ShortText { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        [LocalizedDisplayName("LABEL_ARTICLE_FULL_TEXT")]
        public string FullText { get; set; }

        [Display(Name = "Дата додання")]
        [DataType(DataType.Date, ErrorMessage = "Введіть дату")]
        public DateTime DataCreated { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("LABEL_ARTICLE_LANGUAGE"),]
        public long LanCode { get; set; }

        [LocalizedDisplayName("LABEL_ARTICLE_TAGS")]

        public string Tag { get; set; }

        public long UserID { get; set; }

        public DateTime? DataModified { get; set; }

        public long? LastModifiedBy { get; set; }

        public IEnumerable<SelectListItem> Languages { get; set; }
    }

    public class ArticleIndex
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Logo { get; set; }
        public string ShortText { get; set; }
        public string Tag { get; set; }
        public DateTime DataCreated { get; set; }
        public string UserName { get; set; }
    }

    public class DetailsModel
    {
        public ArticleDetailsModel ArticleDetails { get; set; }
        public IEnumerable<ArticleDetailsModel> ArticleListPopular { get; set; }
    }

    public class ArticleDetailsModel
    {
        public int ID { get; set; }
        public string title { get; set; }
        public string logo { get; set; }
        public string short_text { get; set; }
        public string full_text { get; set; }
        public System.DateTime data_created { get; set; }
        public long LanCode { get; set; }
        public string tag { get; set; }
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
        public int Rating { get; set; }
        public bool Active { get; set; }
        public List<SelectListItem> Languages { get; set; }
    }
    public class ArticleActivate
    {
        public ArticleModel Article { get; set; }
        public bool Activate { get; set; }
    }

    public class ArticleTranslateModel
    {
        public ArticleDetailsModel Article { get; set; }
        public List<SelectListItem> Languages { get; set; }
        public string[] SelectedLanguages { get; set; }

        public IEnumerable<ArticleEditTranslate> ArticleEditTranslate { get; set; }
    }

    public class ArticleEditTranslate
    {
        public int ID { get; set; }
        public int LanCode { get; set; }
        public string LanName { get; set; }

        [Required]
        [StringLength(55)]
        [LocalizedDisplayName("LABEL_ARTICLE_TITLE")]
        public string Title { get; set; }

        [UIHint("tinymce_jquery_minimal"), AllowHtml]
        [LocalizedDisplayName("LABEL_ARTICLE_SHORT_TEXT")]
        [StringLength(300, ErrorMessage = "Field {0} must be minimum={2}, maximum{1}", MinimumLength = 10)]
        public string ShortText { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        [LocalizedDisplayName("LABEL_ARTICLE_FULL_TEXT")]
        public string FullText { get; set; }
    }

    public class PopularPage
    {
        public IPagedList<ArticleDetailsModel> Popular { get; set; }
        public IEnumerable<ArticleDetailsModel> New { get; set; }
    }

    public class NewrPage
    {
        public IEnumerable<ArticleDetailsModel> Popular { get; set; }
        public IPagedList<ArticleDetailsModel> New { get; set; }
    }
    public class ArticleSearchModel
    {
        public IEnumerable<ArticleDetailsModel> Result { get; set; }
        public IEnumerable<ArticleDetailsModel> ArticlePopular { get; set; }
    }
}




