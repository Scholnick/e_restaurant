﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public class SearchModel
    {
        public string SearchText { get; set; }
        public IEnumerable<RestaurantStartPageModel> Restaurants { get; set; }
        public int TotalRestaurants { get; set; }

        public IEnumerable<SearchCityResultModel> Cities { get; set; }
        public int TotalCities { get; set; }

        public IEnumerable<ArticleDetailsModel> Articles { get; set; }
        public int TotalArticles { get; set; }

        public IEnumerable<ArticleDetailsModel> ArticlesPopular { get; set; }
    }

    public class SearchFormModel
    {
        public string q { get; set; }
    }

    public class SearchCityResultModel
    {
        public string City { get; set; }
        public string Country { get; set; }
    }
}