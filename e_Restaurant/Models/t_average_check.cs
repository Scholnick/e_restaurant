//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace e_Restaurant.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class t_average_check
    {
        public long user_id { get; set; }
        public decimal amount_check_min { get; set; }
        public decimal amount_check_max { get; set; }
    
        public virtual t_user t_user { get; set; }
    }
}
