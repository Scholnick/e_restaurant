﻿using e_Restaurant.Repositories;
using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace e_Restaurant.Models
{
    public class ChangePasswordModel
    {
        [RequiredLocalized]
        [DataType(DataType.Password)]
        [LocalizedDisplayName("LABEL_CURRENTPASSWORD")]
        public string CurrentPassword { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("LABEL_NEWPASSWORD")]
        [DataType(DataType.Password)]
        [StringLengthLocalized(100, "COMMON_PASSWORD_LENGTH", 6)]
        public string NewPassword { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("LABEL_CONFIRM_NEWPASSWORD")]
        [DataType(DataType.Password)]
        [CompareLocalizedAttribute("NewPassword", "COMMON_PASSWORD_DONT_MATCH")]
        public string ConfirmNewPasword { get; set; }
        public ChangePasswordModel()
        {

        }
    }

    public class LogOnModel
    {
        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_EMAIL_ADDRESS")]
        [RegularExpressionLocalizedAttribute(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", "COMMON_WRONG_EMAIL")]
        public string UserEmail { get; set; }

        [RequiredLocalized]
        [DataType(DataType.Password)]
        [LocalizedDisplayName("COMMON_PASSWORD")]
        public string Password { get; set; }

        [LocalizedDisplayName("LABEL_REMEMBERME")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_FIRST_NAME")]
        public string FirstName { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_LAST_NAME")]
        public string LastName { get; set; }

        [RequiredLocalized]
        [DataType(DataType.EmailAddress)]
        [LocalizedDisplayName("COMMON_EMAIL_ADDRESS")]
        [RegularExpressionLocalizedAttribute(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", "COMMON_WRONG_EMAIL")]
        [Remote("ValidateEmail", "Account", ErrorMessage = "Email already exist")]
        public string Email { get; set; }

        [RequiredLocalized]
        [StringLengthLocalized(100, "COMMON_PASSWORD_LENGTH", 6)]
        [DataType(DataType.Password)]
        [LocalizedDisplayName("COMMON_PASSWORD")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [LocalizedDisplayName("COMMON_CONFIRM_PASSWORD")]
        [CompareLocalizedAttribute("Password", "COMMON_PASSWORD_DONT_MATCH")]
        public string ConfirmPassword { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_COUNTRY")]
        public long Country { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

        [LocalizedDisplayName("COMMON_CITY")]
        public int? CityID { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }
                
        public string ActivateGuid { get; set; }        
    }

    public class ActivateUserModel
    {
        public string Guid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActivate { get; set; }
        public bool IsEmail { get; set; }
    }

    public class IdentityUserModel
    {
        public long UserID { get; set; }
        public string FullUserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }

    public class ProfileModel
    {
        [RequiredLocalized]
        public long UserID { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_FIRST_NAME")]
        public string FirstName { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_LAST_NAME")]
        public string LastName { get; set; }


        [LocalizedDisplayName("LABEL_PhoneNumber")]
        [RegularExpressionLocalizedAttribute(@"^\d+$", "COMMON_ERROR_ONLY_DIGITAL")]
        public string PhoneNumber { get; set; }

        public string PhoneCode { get; set; }
        public IEnumerable<SelectListItem> ListPhoneCode { get; set; }

        [LocalizedDisplayName("LABEL_BIRTHDAY")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BirthDay { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("LABEL_SEX")]
        public int? Sex { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_COUNTRY")]
        public long Country { get; set; }

        [LocalizedDisplayName("COMMON_CITY")]        
        public int? CityID { get; set; }

        public string ActivateGuid { get; set; }
        public bool SubscribeOffers { get; set; }
        public bool SubscribeBestDeals { get; set; }
        public bool SubscribeNews { get; set; }

        public IEnumerable<SelectListItem> CountryList { get; set; }
        public IEnumerable<SelectListItem> CityList { get; set; }

        public List<SelectListItem> SexList { get; set; }
        public ChangePasswordModel ChangePassword { get; set; }
        public ChangeEmailModel ChangeEmail { get; set; }
        public DeleteAccountModel DeleteAccount { get; set; }

        public string Email { get; set; }
    }

    public class ChangeEmailModel
    {
        [RequiredLocalized]
        [LocalizedDisplayName("LABEL_NEW_EMAIL")]
        [RegularExpressionLocalizedAttribute(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", "COMMON_WRONG_EMAIL")]
        [Remote("ValidateEmail", "Account", ErrorMessage = "Email already exist")]
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string Guid { get; set; }
        [LocalizedDisplayName("LABEL_CURRENT_EMAIL")]
        public string OldEmail { get; set; }

        public ChangeEmailModel()
        {

        }
    }

    public class DeleteAccountModel
    {
        [RequiredLocalized]
        [DataType(DataType.Password)]
        [LocalizedDisplayName("COMMON_PASSWORD")]
        [Remote("CheckCurrentPassword", "Account", ErrorMessage = "Wrong password")]
        public string CurrentPassword { get; set; }
    }

    public class ForgotPaswordModel
    {
        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_EMAIL_ADDRESS")]
        [RegularExpressionLocalizedAttribute(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", "COMMON_WRONG_EMAIL")]
        public string Email { get; set; }
    }

    public class MyPreferencesModel
    {
        public MyRestaurantType RestaurantType { get; set; }
        public string[] SelectedRestaurantType { get; set; }

        public MyCuisineType CuisineType { get; set; }
        public string[] SelectedCuisineType { get; set; }

        public MyService Services { get; set; }
        public string[] SelectedServices { get; set; }

        public MyEntertainment Entertainment { get; set; }
        public string[] SelectedEntertainment { get; set; }

        public MyRooms Rooms { get; set; }
        public string[] SelectedRooms { get; set; }

        public MySpecialOffers SpecialOffers { get; set; }
        public string[] SelectedSpecialOffers { get; set; }

        public MySpecialFeature SpecialFeatures { get; set; }
        public string[] SelectedSpecialFeatures { get; set; }

        public MyEvent Events { get; set; }
        public string[] SelectedEvents { get; set; }

        public MyPayment Payments { get; set; }
        public string[] SelectedPayments { get; set; }

        public List<SelectListItem> AverageCheck { get; set; }
        public string[] SelectedAverageCheck { get; set; }

        public MyWorkingHours WorkingHours { get; set; }

        public MyPreferencesSelectedModel SelectedPreferences { get; set; }
    }

    public class MyRestaurantType
    {
        public List<SelectListItem> RestaurantType { get; set; }

        public MyRestaurantType()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            RestaurantType = preferencesRepo.GetRestaurantType();
        }
    }

    public class MyCuisineType
    {
        public List<SelectListItem> CuisineType { get; set; }

        public MyCuisineType()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            CuisineType = preferencesRepo.GetCuisineType();
        }
    }

    public class MyService
    {
        public List<SelectListItem> Services { get; set; }

        public MyService()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            Services = preferencesRepo.GetServices();
        }
    }

    public class MyEntertainment
    {
        public List<SelectListItem> Entertainment { get; set; }
        public MyEntertainment()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            Entertainment = preferencesRepo.GetEntertainment();
        }
    }

    public class MyRooms
    {
        public List<SelectListItem> Rooms { get; set; }
        public MyRooms()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            Rooms = preferencesRepo.GetRooms();
        }
    }

    public class MySpecialOffers
    {
        public List<SelectListItem> SpecialOffers { get; set; }
        public MySpecialOffers()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            SpecialOffers = preferencesRepo.GetSpecialOffers();
        }
    }

    public class MySpecialFeature
    {
        public List<SelectListItem> SpecialReatures { get; set; }
        public MySpecialFeature()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            SpecialReatures = preferencesRepo.GetSpecialFeatures();
        }
    }

    public class MyEvent
    {
        public List<SelectListItem> Events { get; set; }
        public MyEvent()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            Events = preferencesRepo.GetEvents();
        }
    }

    public class MyPayment
    {
        public List<SelectListItem> Payments { get; set; }
        public MyPayment()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            Payments = preferencesRepo.GetPayments();
        }
    }

    public class MyWorkingHours
    {
        public bool ByHours { get; set; }
        public bool ByTwentyFourHours { get; set; }
        public string BeginHours { get; set; }
        public string EndHours { get; set; }

        public MyWorkingHours()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            var myWorkinghours = preferencesRepo.GetWorkingHours();
            if (myWorkinghours != null)
            {
                BeginHours = myWorkinghours.BeginHours;
                EndHours = myWorkinghours.EndHours;
                ByTwentyFourHours = myWorkinghours.ByTwentyFourHours;
            }
        }
    }

    public class MyPreferencesSelectedModel
    {
        public List<SelectedPreferences> SelectedPreferences { get; set; }

        public string RestaurantTypeSelected { get; set; }
        public string CuisineTypeSelected { get; set; }
        public string ServicesSelected { get; set; }
        public string EntertainmentSelected { get; set; }
        public string RoomsSelected { get; set; }
        public string SpecialOffersSelected { get; set; }
        public string SpecialFeaturesSelected { get; set; }
        public string EventsSelected { get; set; }
        public string PaymentSelected { get; set; }
        public string WorkingHours { get; set; }
        public string IsCleanAll { get; set; }

        public MyPreferencesSelectedModel()
        {
            MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
            SelectedPreferences = preferencesRepo.GetSelectedPreferences();
        }
    }

    public class SelectedPreferences
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string TypePreferences { get; set; }
        public string TargetPreferences { get; set; }
    }
}
