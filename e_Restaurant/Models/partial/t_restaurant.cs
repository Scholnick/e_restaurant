﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Restaurant.Common;
using System.ComponentModel.DataAnnotations;

namespace e_Restaurant.Models
{
    public partial class t_restaurant
    {
        string browserLanguage = COM.BrowserData.Language;

        public string TitleTranslation
        {
            get
            {
                return
                    this.t_rest_translation.Count() > 0
                        ? this.t_rest_translation.Where(r => r.t_language.lan_code.ToLower() == COM.BrowserData.Language).Select(r => r.translation_resttitle).FirstOrDefault()
                        : this.rest_title;
            }
        }

        public string LogoFullPath
        {
            get
            {
                return ImageLink.Restaurant.Logo(this.rest_ref, this.rest_logo);
            }
        }

        public string AdditionalImageFullPath
        {
            get
            {
                return ImageLink.Restaurant.Logo(this.rest_ref, this.rest_pict_more_data, true);
            }
        }

        public string FullDescription
        {
            get
            {
                var translit = this.t_rest_translation.AsEnumerable();
                if (translit != null && translit.Any())
                {
                    return this.t_rest_translation.Where(t => t.t_language.lan_google_code == browserLanguage)
                            .Select(d => d.translation_restdesc).FirstOrDefault();
                }
                else
                {
                    return rest_desc;
                }
            }
        }

    }
}