﻿
using e_Restaurant.Common;
namespace e_Restaurant.Models
{
    public partial class t_payment
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(payment_dict_code);
            }
        }
    }
}