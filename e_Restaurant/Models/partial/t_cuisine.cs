﻿using e_Restaurant.Common;
using System;

namespace e_Restaurant.Models
{
    public partial class t_cuisine
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(cuisine_dict_code);
            }
        }
    }
}