﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public partial class t_rest_type
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(type_dict_code);
            }
        }
    }
}