﻿using e_Restaurant.Common;

namespace e_Restaurant.Models
{
    public partial class t_offer
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(offer_dict_code);
            }
        }
    }
}