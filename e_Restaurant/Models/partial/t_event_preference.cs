﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public partial class t_event_preference
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(eventpr_dict_code);
            }
        }
    }
}