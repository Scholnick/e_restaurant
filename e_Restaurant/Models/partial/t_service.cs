﻿using e_Restaurant.Common;

namespace e_Restaurant.Models
{
    public partial class t_service
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(service_dict_code);
            }
        }
    }
}