﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public partial class t_city
    {

        public string TranslateName
        {

            get
            {
                var browserLanguage = COM.BrowserData.Language;
                return this.t_city_translation.Where(t=>t.t_language.lan_google_code == browserLanguage).Any()
                    ? this.t_city_translation.Where(t=>t.t_language.lan_google_code == browserLanguage).Select(t=>t.city_name).FirstOrDefault()
                    : this.city_name;
            }
        }
    }
}