﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public partial class t_country
    {
        public string TranslateName
        {
            get
            {
                var browserLanguage = COM.BrowserData.Language;
                return this.t_country_translation.Where(t => t.lan_code == browserLanguage).Any()
                    ? this.t_country_translation.Where(t => t.lan_code == browserLanguage).Select(t => t.country_name).FirstOrDefault()
                    : this.country_name;
            }
        }
    }
}