﻿using e_Restaurant.Common;

namespace e_Restaurant.Models
{
    public partial class t_room
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(room_dict_code);
            }
        }
    }
}