﻿using e_Restaurant.Common;

namespace e_Restaurant.Models
{
    public partial class t_feature
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(feature_dict_code);
            }
        }
    }
}