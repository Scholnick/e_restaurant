﻿using e_Restaurant.Common;

namespace e_Restaurant.Models
{
    public partial class t_entertaiment
    {
        public string TranslateName
        {
            get
            {
                return COM.GetTranslation(entertaiment_dict_code);
            }
        }
    }
}