﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public partial class t_unit
    {
        public string FulPathLogo
        {
            get
            {
                return ImageLink.Unit.Image(this.t_restaurant.rest_ref, this.art_picture_url);
            }
        }

        public string TranslateName
        {
            get
            {
                var browserLang = COM.BrowserData.Language;
                return this.t_unit_translate.Where(u => u.t_language.lan_google_code.ToLower() == browserLang).Any()
                        ? this.t_unit_translate.Where(u => u.t_language.lan_google_code.ToLower() == browserLang).Select(u => u.unittranslate_name).FirstOrDefault()
                        : this.unit_name;
            }
        }

        public string NativeName
        {
            get
            {
                return this.t_unit_translate.Where(u => u.t_language.lan_id == u.t_unit.t_restaurant.rest_lan_id).Any()
                        ? this.t_unit_translate.Where(u => u.t_language.lan_id == u.t_unit.t_restaurant.rest_lan_id).Select(u => u.unittranslate_name).FirstOrDefault()
                        : string.Empty;
            }
        }

        public string TranslateDescription
        {
            get
            {
                var browserLang = COM.BrowserData.Language;
                if (this.t_unit_translate.Where(u => u.t_language.lan_google_code.ToLower() == browserLang).Any())
                    return this.t_unit_translate.Where(u => u.t_language.lan_google_code.ToLower() == browserLang).Select(u => u.unittranslate_desc).FirstOrDefault();
                else
                    return this.unit_desc;
            }
        }

        public string NativeDescription
        {
            get
            {
                if (this.t_unit_translate.Where(u => u.t_language.lan_id == u.t_unit.t_restaurant.rest_lan_id).Any())
                    return this.t_unit_translate.Where(u => u.t_language.lan_id == u.t_unit.t_restaurant.rest_lan_id).Select(u => u.unittranslate_desc).FirstOrDefault();
                else
                    return string.Empty;
            }
        }
    }
}