﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using e_Restaurant.Models;

namespace e_Restaurant.Models
{
    public partial class t_article
    {
        public string LogoFullPath
        {
            get
            {
                return ImageLink.Article.FullPathLogo(this.ID, this.logo);
            }
        }
    }
}