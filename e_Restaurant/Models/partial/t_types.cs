﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Restaurant.Common;

namespace e_Restaurant.Models
{
    public partial class t_types
    {
        public string TranslateName
        {
            get
            {
                var browserLanguage = COM.BrowserData.Language;
                return this.t_types_translate.Where(ttr => ttr.t_language.lan_google_code == browserLanguage).Any()
                       ? this.t_types_translate.Where(ttr => ttr.t_language.lan_google_code == browserLanguage)
                           .Select(ttr => ttr.typestranslate_name)
                           .FirstOrDefault()
                       : this.type_name;
            }
        }

        public string NativeName
        {
            get
            {
                if (this.t_types_translate.Where(ttr => ttr.t_language.lan_id == ttr.t_types.t_restaurant.rest_lan_id).Any())
                    return this.t_types_translate.Where(ttr => ttr.t_language.lan_id == ttr.t_types.t_restaurant.rest_lan_id)
                               .Select(ttr => ttr.typestranslate_name).FirstOrDefault();
                else
                    return string.Empty;
            }
        }
    }
}