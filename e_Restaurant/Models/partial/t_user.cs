﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public partial class t_user
    {
        public string PhoneNumber
        {
            get
            {
                if (user_phone != null && user_phone.IndexOf("-") != -1)
                    return user_phone.Split('-')[1];

                return user_phone;
            }
        }

        public string PhoneCode
        {
            get
            {
                return user_phone != null && user_phone.IndexOf("-") != -1 && user_phone != null ? user_phone.Split('-')[0] : "";
            }
        }
    }
}