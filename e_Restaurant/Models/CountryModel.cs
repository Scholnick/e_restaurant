﻿using e_Restaurant.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public class CountryModel
    {
        [Required]
        [Display(Name = "ID")]
        public long ID { get; set; }

        [Required]
        [Display(Name = "Country name")]
        public string CountryName { get; set; }
    }

    public class CreateCountryModel
    {
        [Required]
        [Display(Name = "ID")]
        public long ID { get; set; }

        [Required]
        [Display(Name = "Country name")]
        public string CountryName { get; set; }

        [Required]
        [Display(Name = "Country code a2")]
        public string CountryCodeA2 { get; set; }
        
        [Required]
        [Display(Name = "Country code a3")]
        public string CountryCodeA3 { get; set; }
        
        [Required]
        [Display(Name = "Country code number")]
        public string CountryCodeNumber { get; set; }
    }

    public class CityModel
    {
        [RequiredLocalized]
        [Display(Name = "ID")]
        public long ID { get; set; }

        [RequiredLocalized]
        [LocalizedDisplayName("COMMON_CITY")]
        public string CityName { get; set; }
    }
}