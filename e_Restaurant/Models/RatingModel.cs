﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public class RatingModel
    {
    }

    public class VoteModel
    {
        public long RestID { get; set; }
        public int RatingCategory { get; set; }
        public decimal Score { get; set; }
        public long UserID { get; set; }
    }

    public class MyRatingModel
    {
        public long RestID { get; set; }
        public int RatingCategory { get; set; }
        public int Score { get; set; }
    }

    public class RatingAverageModel
    {
        public long RestID { get; set; }
        public int RatingCategory { get; set; }
        public decimal AverageRate { get; set; }
    }
}