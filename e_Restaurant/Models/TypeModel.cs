﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Models
{
    public class TypeModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string NativeName { get; set; }
        public string Icon { get; set; }
        public List<TypeModel> SubTypeList { get; set; }

        public TypeModel()
        {
            SubTypeList = new List<TypeModel>();
        }
    }
}