﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Restaurant.Models;
using System.Security.Cryptography;
using System.Web.Security;
using e_Restaurant.Common;
using System.Web.Mvc;
using System.Text;

namespace e_Restaurant.Repositories
{
    public class UserRepository
    {
        private local db = new local();

        public void CreateUser(RegisterModel user)
        {
            var newUser = new t_user()
            {
                user_firstname = user.FirstName,
                user_lastname = user.LastName,
                user_email = user.Email,
                user_passsalt = CreateSalt(),
                user_active = false,
                user_activateguid = Guid.NewGuid().ToString(),
                user_type = 0,
                country_id = user.Country,
                city_id = user.CityID.HasValue && user.CityID.Value != 0 ? user.CityID.Value : (int?)null,
                user_createddate = DateTime.Now,
                user_subscribe_bestdeals = true,
                user_subscribe_news = true,
                user_subscribe_offers = true,
                role_id = db.t_role.Where(r => r.role_name.ToLower() == "user").FirstOrDefault().role_id
            };
            newUser.user_password = CreatePasswordHash(user.Password, newUser.user_passsalt);
            db.t_user.Add(newUser);
            db.SaveChanges();

            List<t_user> notActive = (from u in db.t_user
                                      where u.user_active == false && u.user_activatedate == null
                                      select u).ToList().Where(u => (DateTime.Now - u.user_createddate).TotalHours >= 24).Select(u => u).ToList<t_user>();

            foreach (t_user userItem in notActive)
            {
                db.t_user.Remove(userItem);
            }
            db.SaveChanges();
            
            user.ActivateGuid = newUser.user_activateguid;
        }

        private static string CreateSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[32];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        private static string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, salt);
            string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "sha1");
            return hashedPwd;
        }

        //check unique email
        public bool IsValidEmail(string email)
        {
            var list =
                (from u in db.t_user
                 where u.user_email == email
                 select u).ToList()
                         .Where(u => (u.user_active == false && u.user_activatedate == null && (DateTime.Now - u.user_createddate).TotalHours <= 24) ||
                                     (u.user_active == true && u.user_activatedate.HasValue && u.user_deleteddate.HasValue && u.user_deleted.HasValue) ||
                                     (u.user_active == true && u.user_activatedate.HasValue)
                                ).ToList();
            return !list.Any();
        }

        public bool ChangePassword(ChangePasswordModel changeModel)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null && user.user_password == CreatePasswordHash(changeModel.CurrentPassword, user.user_passsalt))
            {
                string passwordSalt = CreateSalt();
                user.user_passsalt = passwordSalt;
                user.user_password = CreatePasswordHash(changeModel.NewPassword, passwordSalt);
                user.user_changeddate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool ChangeEmail(ChangeEmailModel changeEmailModel)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null && user.user_active)
            {
                user.user_emailchange = changeEmailModel.Email;
                user.user_activateguid = Guid.NewGuid().ToString();
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public ActivateUserModel GetUserForActivate(string activateGuid, bool IsEmail = false)
        {
            var userActivating = db.t_user.Where(u => u.user_activateguid == activateGuid).FirstOrDefault();
            ActivateUserModel userActivate = new ActivateUserModel();
            if (userActivating != null)
            {
                userActivate.Guid = userActivating.user_activateguid;
                switch (IsEmail)
                {
                    case false:
                        if (!userActivating.user_activatedate.HasValue && string.IsNullOrEmpty(userActivating.user_emailchange))
                        {
                            userActivating.user_active = true;
                            userActivating.user_activatedate = DateTime.Now;
                            userActivating.user_activateguid = null;
                            db.SaveChanges();
                        }
                        break;
                    case true:
                        if (userActivating.user_activatedate.HasValue && !string.IsNullOrEmpty(userActivating.user_emailchange))
                        {
                            userActivating.user_email = userActivating.user_emailchange;
                            userActivating.user_emailchange = null;
                            userActivating.user_activateguid = null;
                            userActivating.user_activatedate = DateTime.Now;
                            db.SaveChanges();
                        }
                        break;
                }

                userActivate.IsEmail = IsEmail;
                userActivate.FirstName = userActivating.user_firstname;
                userActivate.LastName = userActivating.user_lastname;
                userActivate.IsActivate = userActivating.user_active;

            }
            return userActivate;
        }

        public bool ValidateUser(string email, string password)
        {
            var user = db.t_user.Where(u => u.user_email == email).FirstOrDefault();
            if (user != null)
            {
                if (user.user_password == CreatePasswordHash(password, user.user_passsalt) &&
                    user.user_active == true &&
                    user.user_activatedate.HasValue &&
                    user.user_deleteddate == null &&
                    user.user_deleted == null)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public IdentityUserModel GetIdentityData(string email)
        {
            return
                db.t_user.Where(u => u.user_email == email)
                .Select(u => new IdentityUserModel
                       {
                           UserID = u.user_id,
                           FirstName = u.user_firstname,
                           LastName = u.user_lastname,
                           FullUserName = u.user_firstname + " " + u.user_lastname,
                           Email = u.user_email,
                           Role = u.t_role.role_name
                       }).FirstOrDefault();
        }

        public string[] GetRole(string userEmail)
        {
            return db.t_user.Where(u => u.user_email == userEmail).Select(u => u.t_role.role_name).ToArray();
        }

        public ProfileModel GetProfileData()
        {
            return db.t_user.Where(u => u.user_id == COM.UserData.ID).ToList()
                            .Select(u => new ProfileModel
                            {
                                UserID = u.user_id,
                                FirstName = u.user_firstname,
                                LastName = u.user_lastname,
                                PhoneNumber = u.PhoneNumber,
                                PhoneCode = u.PhoneCode,
                                BirthDay = u.user_birthday,
                                Sex = u.user_sex,
                                Country = u.country_id,
                                CityID = u.city_id.HasValue && u.city_id.Value != 0 ? u.city_id : (int?)null,
                                ActivateGuid = u.user_activateguid,
                                Email = u.user_email,
                                SubscribeOffers = u.user_subscribe_offers,
                                SubscribeBestDeals = u.user_subscribe_bestdeals,
                                SubscribeNews = u.user_subscribe_news
                            }).FirstOrDefault();
        }

        public void SaveProfileData(ProfileModel profileData)
        {
            var user = db.t_user.Where(u => u.user_email == COM.UserData.Email).FirstOrDefault();
            if (user != null)
            {
                user.user_firstname = profileData.FirstName;
                user.user_lastname = profileData.LastName;
                user.user_phone = (!string.IsNullOrEmpty(profileData.PhoneCode) ? profileData.PhoneCode + "-" : "") + profileData.PhoneNumber;
                user.user_birthday = profileData.BirthDay;
                user.user_sex = profileData.Sex;
                user.country_id = profileData.Country;
                user.city_id = profileData.CityID.Value == 0 || !profileData.CityID.HasValue ? (int?)null : profileData.CityID.Value;
                user.user_changeddate = DateTime.Now;
                user.user_subscribe_bestdeals = profileData.SubscribeBestDeals;
                user.user_subscribe_news = profileData.SubscribeNews;
                user.user_subscribe_offers = profileData.SubscribeOffers;
                db.SaveChanges();
            }
        }
        //LABEL_MALE 0=male
        //LABEL_FEMALE 1=female
        public List<SelectListItem> GetSexList()
        {
            return new List<SelectListItem>() 
                {
                 new SelectListItem { Text = COM.GetTranslation("LABEL_MALE"), Value = "0" },
                 new SelectListItem { Text = COM.GetTranslation("LABEL_FEMALE"), Value = "1" }
                };
        }

        public bool DeleteAccount(DeleteAccountModel deleteModel)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null && user.user_password == CreatePasswordHash(deleteModel.CurrentPassword, user.user_passsalt))
            {
                user.user_deleted = COM.UserData.ID;
                user.user_deleteddate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public static string GeneratePassword(int length)
        {
            const string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(validChars[rnd.Next(validChars.Length)]);
            }
            return res.ToString();
        }

        public bool ResetPassword(ForgotPaswordModel forgotModel, string password, out string UserNameResetPassword)
        {
            bool result = false;
            t_user user = db.t_user.Where(u => u.user_email == forgotModel.Email).FirstOrDefault();
            if (user != null)
            {
                string passwordSalt = CreateSalt();
                user.user_password = CreatePasswordHash(password, passwordSalt);
                user.user_passsalt = passwordSalt;
                user.user_changeddate = DateTime.Now;
                db.SaveChanges();
                result = true;
                UserNameResetPassword = user.user_firstname;
            }
            else
                UserNameResetPassword = "";

            return result;
        }


    }
}