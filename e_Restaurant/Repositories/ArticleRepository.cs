﻿using e_Restaurant.Common;
using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Data;
using PagedList;

namespace e_Restaurant.Repositories
{
    public class ArticleRepository
    {
        private local db;
        public ArticleRepository()
        {
            db = new local();
        }

        public LanguageRepository langRepo = new LanguageRepository();

        public IEnumerable<ArticleDetailsModel> GetList()
        {
            string browserLanguage = COM.BrowserData.Language;
            var langID = (from l in db.t_language where l.lan_google_code == browserLanguage select l.lan_id).FirstOrDefault();

            var list = from art in db.t_article.ToList()
                       select new ArticleDetailsModel()
                       {
                           ID = art.ID,
                           logo = art.LogoFullPath,
                           title = art.t_article_translate.Where(l => l.lan_code == langID).Any()
                                        ? art.t_article_translate.Where(l => l.lan_code == langID).FirstOrDefault().title
                                         : art.title,
                           short_text = art.t_article_translate.Where(l => l.lan_code == langID).Any()
                                        ? art.t_article_translate.Where(l => l.lan_code == langID).FirstOrDefault().schort_text
                                        : art.schort_text,
                           data_created = art.data_created,
                           LanCode = art.lan_code,
                           tag = art.tag,
                           UserID = art.user_id,
                           Languages = langRepo.GetListForCurrArt(art.ID),
                           Active = art.active
                       };

            return list;
        }

        public IEnumerable<ArticleDetailsModel> MyArticle(int id)
        {
            return GetList().Where(a => a.UserID == id);
        }

        public static string RemoveHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        private static void SaveLogoFromArticle(ArticleModel article, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                string fileName = "";
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ImageLink.Article.UploadDirectory)))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ImageLink.Article.UploadDirectory));


                fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(HttpContext.Current.Server.MapPath(ImageLink.Article.UploadDirectory) + fileName);
                if (fileName != null)
                    file.SaveAs(path);
                article.Logo = fileName;
            }
        }

        public int Create(ArticleModel article, HttpPostedFileBase file)
        {
            SaveLogoFromArticle(article, file);

            var newarticle = new t_article()
            {
                title = HttpUtility.HtmlDecode(article.Title),
                logo = article.Logo,
                schort_text = HttpUtility.HtmlDecode(article.ShortText),
                full_text = HttpUtility.HtmlDecode(article.FullText),
                data_created = DateTime.Now,
                user_id = COM.UserData.ID,
                lan_code = article.LanCode,
                tag = HttpUtility.HtmlDecode(article.Tag)
            };
            db.t_article.Add(newarticle);
            db.SaveChanges();
            MoveLogo(article.Logo, newarticle.ID);
            if (!string.IsNullOrEmpty(article.Logo))
            {
                List<t_article> ls = new List<t_article>();
                ls = (from i in db.t_article select i).ToList();
                newarticle.logo = ls.Last().ID + "_" + article.Logo;
                db.SaveChanges();
            }

            return newarticle.ID;
        }

        public void Update(ArticleModel article, HttpPostedFileBase file)
        {
            t_article entry = (from e in db.t_article where article.ID == e.ID select e).FirstOrDefault();
            if (entry != null)
            {
                SaveLogoFromArticle(article, file);
                entry.logo = !string.IsNullOrEmpty(article.Logo) ? article.Logo : entry.logo;
                entry.title = article.Title;
                entry.schort_text = article.ShortText;
                entry.full_text = article.FullText;
                entry.lan_code = article.LanCode;
                entry.tag = article.Tag;
                entry.data_modified = DateTime.Now;
                entry.last_modified_by = COM.UserData.ID;
                db.SaveChanges();
                MoveLogo(article.Logo, entry.ID);
                if (!string.IsNullOrEmpty(article.Logo))
                {
                    entry.logo = article.ID + "_" + article.Logo;
                    db.SaveChanges();
                }
            }
        }

        private static void MoveLogo(string logoFileName, int articleID)
        {
            if (!string.IsNullOrEmpty(logoFileName))
            {
                string oldFullPath = Path.Combine(HttpContext.Current.Server.MapPath(ImageLink.Article.UploadDirectory) + logoFileName);
                string newPath = Path.Combine(HttpContext.Current.Server.MapPath(ImageLink.Article.UploadDirectory) + articleID);
                string newFullPath = Path.Combine(newPath, articleID + "_" + logoFileName);
                if (!Directory.Exists(newPath))
                    Directory.CreateDirectory(newPath);
                File.Copy(oldFullPath, newFullPath, true);
                File.Delete(oldFullPath);
            }
        }

        public void AddTranslate(ArticleTranslateModel article, int id)
        {
            var baseArticle = db.t_article.Where(a => a.ID == id).FirstOrDefault();
            if (article.SelectedLanguages != null)
            {
                foreach (string language in article.SelectedLanguages)
                {
                    try
                    {
                        string titleText = COM.GetGoogleTranslate(RemoveHTML(baseArticle.title), baseArticle.t_language.lan_google_code, language);
                        string shortText = COM.GetGoogleTranslate(RemoveHTML(baseArticle.schort_text), baseArticle.t_language.lan_google_code, language);
                        string fullText = COM.GetGoogleTranslate(RemoveHTML(baseArticle.full_text), baseArticle.t_language.lan_google_code, language);
                        var newTranslate = new t_article_translate()
                            {
                                ID = baseArticle.ID,
                                lan_code = langRepo.GetLanguageId(language),
                                title = titleText.Length > 55 ? titleText.Substring(0, 54) : titleText,
                                schort_text = shortText.Length > 300 ? shortText.Substring(0, 299) : shortText,
                                full_text = fullText
                            };
                        db.t_article_translate.Add(newTranslate);
                        db.SaveChanges();
                    }
                    catch { }
                }
            }
        }

        public IEnumerable<ArticleEditTranslate> GetListArticleTranslate(int id)
        {
            var artList = from ar in db.t_article_translate.Where(i => i.ID == id).ToList()
                          join lang in db.t_language on ar.lan_code equals lang.lan_id
                          select new ArticleEditTranslate()
                          {
                              ID = ar.ID,
                              LanCode = ar.lan_code,
                              Title = ar.title,
                              ShortText = ar.schort_text,
                              FullText = ar.full_text,
                              LanName = lang.lan_name
                          };
            return artList;
        }

        public ArticleEditTranslate GetOneArticleTranslate(int id, int langId)
        {
            var antryArticle = (from transl in db.t_article_translate
                                where transl.ID == id && transl.lan_code == langId
                                select new ArticleEditTranslate
                                {
                                    ID = transl.ID,
                                    Title = transl.title,
                                    FullText = transl.full_text,
                                    ShortText = transl.schort_text,
                                    LanCode = transl.lan_code
                                }).FirstOrDefault();

            return antryArticle;
        }

        public void Update(ArticleEditTranslate article)
        {
            t_article_translate entry = (from e in db.t_article_translate where article.ID == e.ID select e).FirstOrDefault();
            if (article != null)
            {
                entry.title = article.Title;
                entry.schort_text = article.ShortText;
                entry.full_text = article.FullText;
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public ArticleDetailsModel Details(int id)
        {
            string brouserLanguage = e_Restaurant.Common.COM.BrowserData.Language;
            var cultures = System.Globalization.CultureInfo.GetCultureInfoByIetfLanguageTag(brouserLanguage);
            var langID = (from l in db.t_language where l.lan_google_code == brouserLanguage select l.lan_id).FirstOrDefault();

            var article = (from art in db.t_article.Where(a => a.ID == id).ToList()
                           join u in db.t_user on art.user_id equals u.user_id into ugr
                           from user in ugr.DefaultIfEmpty()
                           select new ArticleDetailsModel()
                           {
                               ID = art.ID,
                               logo = art.LogoFullPath,
                               title = art.t_article_translate.Where(l => l.lan_code == langID).Any()
                                            ? art.t_article_translate.Where(l => l.lan_code == langID).FirstOrDefault().title
                                            : art.title,
                               short_text = art.t_article_translate.Where(l => l.lan_code == langID).Any()
                                            ? art.t_article_translate.Where(l => l.lan_code == langID).FirstOrDefault().schort_text
                                           : art.schort_text,
                               full_text = HttpUtility.HtmlDecode(art.t_article_translate.Where(l => l.lan_code == langID).Any()
                                            ? art.t_article_translate.Where(l => l.lan_code == langID).FirstOrDefault().full_text
                                            : art.full_text),
                               data_created = art.data_created,
                               tag = HttpUtility.HtmlDecode(art.tag),
                               LanCode = art.lan_code,
                               UserName = user != null ? user.user_firstname : "",
                               Rating = art.rating,
                               UserID = art.user_id
                           }).FirstOrDefault();

            return article;
        }

        public void UpdateCountArticle(int id)
        {
            var article = (from a in db.t_article where a.ID == id select a).FirstOrDefault();
            if (article != null)
            {
                article.rating += 1;
                db.Entry(article).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public ArticleModel FindById(int id)
        {
            var article = (from art in db.t_article
                           where art.ID == id
                           select new ArticleModel()
             {
                 ID = art.ID,
                 Title = art.title,
                 ShortText = art.schort_text,
                 FullText = art.full_text,
                 LanCode = art.lan_code,
                 Logo = art.logo,
                 Tag = art.tag,
                 DataModified = art.data_modified,
                 LastModifiedBy = art.last_modified_by,
                 UserID = art.user_id,
                 DataCreated = art.data_created
             }).FirstOrDefault();
            if (article != null)
            {
                article.Languages = langRepo.GetListForEdit();
            }
            return article;
        }

        public void Delete(int id)
        {
            var article = (from a in db.t_article where a.ID == id select a).FirstOrDefault();
            if (article != null)
            {
                db.t_article.Remove(article);
                db.SaveChanges();
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public IEnumerable<ArticleDetailsModel> ListArticleDetails()
        {
            string browserLanguage = COM.BrowserData.Language;
            var langID = (from l in db.t_language where l.lan_google_code == browserLanguage select l.lan_id).FirstOrDefault();
            var list = from art in db.t_article.Where(a => a.lan_code == langID || a.t_article_translate.Where(s => s.lan_code == langID).Any()).ToList()
                       join u in db.t_user on art.user_id equals u.user_id into ugr
                       from user in ugr.DefaultIfEmpty()
                       select new ArticleDetailsModel()
                       {
                           ID = art.ID,
                           logo = art.LogoFullPath,
                           title = art.t_article_translate.Where(l => l.lan_code == langID).Any()
                                       ? art.t_article_translate.Where(l => l.lan_code == langID).FirstOrDefault().title
                                       : art.title,
                           short_text = art.t_article_translate.Where(l => l.lan_code == langID).Any()
                                        ? art.t_article_translate.Where(l => l.lan_code == langID).FirstOrDefault().schort_text
                                        : art.schort_text,
                           data_created = art.data_created,
                           LanCode = art.lan_code,
                           Rating = art.rating,
                           tag = art.tag,
                           UserID = art.user_id,
                           Languages = langRepo.GetListForCurrArt(art.ID),
                           UserRole = user.t_role.role_name,
                           Active = art.active
                       };
            return list;
        }

        public IEnumerable<ArticleDetailsModel> ArticleNew()
        {
            var listRating = ListArticleDetails().Where(a => a.Active == true).OrderByDescending(a => a.data_created);
            return listRating.ToList();
        }

        public IEnumerable<ArticleDetailsModel> ArticlePopular()
        {
            var listRating = ListArticleDetails().Where(a => a.Active == true).OrderByDescending(a => a.Rating);
            return listRating.ToList();
        }

        internal void Dispose()
        {
            db.Dispose();
        }

        public void Activate(int id)
        {
            var article = (from a in db.t_article where a.ID == id select a).FirstOrDefault();
            if (article != null)
            {
                article.active = true;
                db.Entry(article).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Deactivate(int id)
        {
            var article = (from a in db.t_article where a.ID == id select a).FirstOrDefault();
            if (article != null)
            {
                article.active = false;
                db.Entry(article).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}