﻿using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Repositories
{
    public class SearchRepository
    {
        private local db;
        public SearchRepository()
        {
            db = new local();
        }

        private readonly RestaurantRepository restaurantRepo = new RestaurantRepository();
        private readonly ArticleRepository articleRepo = new ArticleRepository();
        
        public SearchModel Search(string searchText)
        {
            var searchResult = new SearchModel() { SearchText = searchText.Trim() };

            //search restaurant in restaurant name
            var searchRestaurant = RestaurantName(searchText);
            searchResult.Restaurants = searchRestaurant.Take(3);
            searchResult.TotalRestaurants = searchRestaurant.Count();

            //search restaurant in city
            var searchCities = Cities(searchText);
            searchResult.Cities = searchCities.Take(3);
            searchResult.TotalCities = searchCities.Count();

            //search articles
            var searchArticles = Articles(searchText);
            searchResult.Articles = searchArticles.Take(3);
            searchResult.TotalArticles = searchArticles.Count();

            searchResult.ArticlesPopular = articleRepo.ArticlePopular();

            return searchResult;
        }

        public IEnumerable<RestaurantStartPageModel> RestaurantName(string search)
        {
            var tmpSearch = search.ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Where(x => x.Length > 2).ToList();

            var restaurantList = restaurantRepo.GetListStartPage()
                .Where(r => tmpSearch.Any(x => r.Name.ToLower().Contains(x)));

            return restaurantList;
        }

        public IEnumerable<SearchCityResultModel> Cities(string city)
        {
            var tmpCity = city.ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Where(x => x.Length > 2).ToList();

            var cityResult = db.t_city.Where(c => c.t_address.Any() && tmpCity.Any(x=>c.city_name.ToLower().Contains(x) ))
                .ToList()
                .Select(c => new SearchCityResultModel() { City = c.TranslateName, Country = c.t_country.TranslateName });

            return cityResult;//restaurantRepo.GetListStartPage().Where(r => tmpCity.Any(x => r.City.ToLower().Contains(x)));
        }

        public IEnumerable<RestaurantStartPageModel> RestaurantByCities(string search)
        {
            var tmpSearch = search.ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Where(x => x.Length > 2).ToList();

            var restaurantList = restaurantRepo.GetListStartPage()
                .Where(r => tmpSearch.Any(x => r.City.ToLower().Contains(x)));

            return restaurantList; 
        }

        public IEnumerable<ArticleDetailsModel> Articles(string search)
        {
            var tmpSearch = search.ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Where(x => x.Length > 2).ToList();

            var articleList = articleRepo.GetList()
                .Where(r => tmpSearch.Any(t => r.title.ToLower().Contains(t)) || 
                            tmpSearch.Any(s=>r.short_text.ToLower().Contains(s)));

            return articleList; 
        }
    }
}