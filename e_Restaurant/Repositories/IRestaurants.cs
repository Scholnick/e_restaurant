﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using e_Restaurant.Models;

namespace e_Restaurant.Repositories
{

    interface IRestaurants<T> : IDisposable
   where T : class
    {
        T GetOneObject(int id); // получение одного объекта по id
        void Create(T item); // создание объекта
        void Update(T item); // обновление объекта
        void Delete(int id); // удаление объекта по id
        void Save();  // сохранение изменений


    }

    interface IRestaurantsStartPage<T> : IDisposable where T : class
    {
        IEnumerable<T> GetListStartPage();
    }
}
