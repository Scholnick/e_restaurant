﻿using e_Restaurant.Common;
using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace e_Restaurant.Repositories
{
    public class CityRepository
    {

        private local db = new local();

        /// <summary>
        /// Dropdownlist
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetList(long countryID)
        {
            string browserLanguage = COM.BrowserData.Language;
            var cityList = db.t_city.Where(c => c.country_id == countryID).ToList()
                .Select(c => new CityModel
                             {
                                 ID = c.city_id,
                                 CityName = c.TranslateName
                             }
                       ).ToList();

            return from c in cityList
                   orderby c.CityName
                   select new SelectListItem
                   {
                       Selected = false,
                       Value = c.ID.ToString(),
                       Text = c.CityName
                   };
        }

        public List<string> GetCityList(int id, string filter)
        {
            return db.t_city.Where(i => i.country_id == id)
             .Where(s => s.city_name.ToLower().StartsWith(filter.ToLower()))
             .OrderBy(a => a.city_name)
             .Select(x => x.city_name).ToList();
        }

        public t_city Get(string cityName)
        {
            string browserLanguage = COM.BrowserData.Language;

            return db.t_city.Where(d => d.city_name == cityName).FirstOrDefault();
            /*c => new CityModel
            {
                ID = c.city_id,
                CityName = c.t_city_translation.Where(l => l.lan_code.ToLower().IndexOf(browserLanguage) != -1).Any()
                             ? c.t_city_translation.Where(l => l.lan_code.ToLower().IndexOf(browserLanguage) != -1).FirstOrDefault().city_name
                             : c.city_name
            });*/
        }

        public t_city Create(t_city t_city)
        {
            new t_city();
            return null;

        }


        /* public KeyValuePair<string, int>[] GetDataList(int id, string filter)
         {
             List<KeyValuePair<string, int>> returnColl = new List<KeyValuePair<string, int>>();
             var q = (from x in db.t_city
                      where (x.country_id == id && x.city_name.ToLower().StartsWith(filter.ToLower()))
                      select new { x.city_id, x.city_name });
             var query = q.AsEnumerable()
                   .Select(item => new KeyValuePair<int, string>(item.city_id, item.city_name))
                   .ToList();
             return returnColl.ToArray();
         }

        /*public string GetList(int b)
        {
            string browserLanguage = COM.BrowserData.Language;
            var list = db.t_city.Select(c => new CityModel
            {
                ID = c.city_id,
                CityName = c.t_city_translation.Where(l => l.lan_code.ToLower().IndexOf(browserLanguage) != -1).Any()
                             ? c.t_city_translation.Where(l => l.lan_code.ToLower().IndexOf(browserLanguage) != -1).FirstOrDefault().city_name
                             : c.city_name
            }).ToList();

           var endlist=   from c in list
                   orderby c.CityName
                   select new SelectListItem
                   {
                       Selected = false,
                       Value = c.ID.ToString(),
                       Text = c.CityName
                   };
           
               // return a;
           return new JavaScriptSerializer().Serialize(endlist);
        }*/
    }
}