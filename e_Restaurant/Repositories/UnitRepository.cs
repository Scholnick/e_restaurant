﻿using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Repositories
{

    public class UnitRepository
    {
        private local db;
        public UnitRepository()
        {
            db = new local();
        }

        public IEnumerable<UnitModel> GetUnitList(long RestaurantID)
        {
            long? firstType = db.t_unit.Where(t => t.unit_active == 1 && t.rest_id == RestaurantID)
                               .OrderBy(t => t.t_types.type_id_parent)
                               .ThenBy(o => o.t_types.type_id)
                               .Select(t => t.type_id).FirstOrDefault();

            return from u in db.t_unit.Where(u => u.rest_id == RestaurantID && u.type_id == firstType && u.unit_active == 1).ToList()
                   select new UnitModel
                   {
                       ID = u.unit_id,
                       Name = HttpUtility.HtmlDecode(u.TranslateName),
                       NativeName = HttpUtility.HtmlDecode(u.NativeName),
                       Description = HttpUtility.HtmlDecode(u.TranslateDescription),
                       NativeDescription = HttpUtility.HtmlDecode(u.NativeDescription),
                       Image = u.FulPathLogo,
                       Price = u.t_price.Select(p => p.price_cost.ToString() + " " + p.t_currency.cur_code_str).FirstOrDefault()
                   };
        }

        public IEnumerable<UnitModel> GetUnitListByID(int TypeID)
        {
            return from u in db.t_unit.Where(t => t.unit_active == 1 && t.type_id == TypeID).ToList()
                   select new UnitModel
                   {
                       ID = u.unit_id,
                       Name = HttpUtility.HtmlDecode(u.TranslateName),
                       NativeName = HttpUtility.HtmlDecode(u.NativeName),
                       Description = HttpUtility.HtmlDecode(u.TranslateDescription),
                       NativeDescription = HttpUtility.HtmlDecode(u.NativeDescription),
                       Image = u.FulPathLogo,
                       Price = u.t_price.Select(p => p.price_cost.ToString() + " " + p.t_currency.cur_code_str).FirstOrDefault()
                   };
        }
    }
}