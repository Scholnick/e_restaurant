﻿using e_Restaurant.Common;
using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_Restaurant.Repositories
{
    public class CountryRepository
    {
        private local db = new local();

        public IEnumerable<SelectListItem> GetList(bool selectDetectedCountry = false)
        {
            string browserLanguage = COM.BrowserData.Language;
            var list = db.t_country
                         .Select(c => new CountryModel
                         {
                             ID = c.country_id,
                             CountryName = c.t_country_translation.Where(l => l.lan_code.ToLower().IndexOf(browserLanguage) != -1).Any()
                                         ? c.t_country_translation.Where(l => l.lan_code.ToLower().IndexOf(browserLanguage) != -1).FirstOrDefault().country_name
                                         : c.country_name
                         }).ToList();

            return from c in list
                   orderby c.CountryName
                   select new SelectListItem
                            {
                                Selected = false,
                                Value = c.ID.ToString(),
                                Text = c.CountryName
                            };

        }

        public IEnumerable<SelectListItem> GetCountryPhoneCodes()
        {
            return db.t_country.Where(p => !string.IsNullOrEmpty(p.country_phonecode)).OrderBy(o => o.country_code_a3).ToList().Select(p => new SelectListItem
                                                                 {
                                                                     Selected = false,
                                                                     Value = p.country_phonecode,
                                                                     Text = p.country_code_a3 + ' ' + p.country_phonecode
                                                                 }
            );
        }
    }
}