﻿using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_Restaurant.Repositories
{
    public class LanguageRepository
    {
        private readonly local db;

        public LanguageRepository()
        {
            db = new local();
        }

        public List<SelectListItem> GetList()
        {
            return (from l in db.t_language.ToList()
                    select new SelectListItem
                    {
                        Selected = false,
                        Text = l.lan_id.ToString(),
                        Value = l.lan_google_code
                    }).ToList();
        }

        public int GetLanguageId(string lan_google_code)
        {
            return Convert.ToInt32((from l in db.t_language where l.lan_google_code == lan_google_code select l.lan_id).FirstOrDefault());
        }

        public List<SelectListItem> GetListForEdit()
        {
            return (from l in db.t_language.ToList()
                    orderby l.lan_name
                    select new SelectListItem
                    {
                        Selected = false,
                        Text = l.lan_name,
                        Value = l.lan_id.ToString()
                    }).ToList();
        }

        public List<SelectListItem> GetListForCurrArt(int id)
        {
            var listLan = from art in db.t_article_translate.ToList()
                          join lan in db.t_language.ToList() on art.lan_code equals lan.lan_id
                          where art.ID == id
                          select new SelectListItem
                          {
                              Selected = false,
                              Text = lan.lan_id.ToString(),
                              Value = lan.lan_name
                          };
            return listLan.ToList();
        }
    }
}