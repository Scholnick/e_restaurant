﻿using e_Restaurant.Common;
using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_Restaurant.Repositories
{
    public class MyPreferencesRepository
    {
        private local db = new local();

        public List<SelectListItem> GetRestaurantType()
        {
            return (from rt in db.t_rest_type.ToList()
                    select new SelectListItem()
                    {
                        Value = rt.type_id.ToString(),
                        Text = rt.TranslateName,
                        Selected = rt.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetCuisineType()
        {
            return (from c in db.t_cuisine.ToList()
                    select new SelectListItem()
                    {
                        Value = c.cuisine_id.ToString(),
                        Text = c.TranslateName,
                        Selected = c.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetServices()
        {
            return (from s in db.t_service.ToList()
                    select new SelectListItem()
                    {
                        Value = s.service_id.ToString(),
                        Text = s.TranslateName,
                        Selected = s.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetEntertainment()
        {
            return (from e in db.t_entertaiment.ToList()
                    select new SelectListItem()
                    {
                        Value = e.entertaiment_id.ToString(),
                        Text = e.TranslateName,
                        Selected = e.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetRooms()
        {
            return (from r in db.t_room.ToList()
                    select new SelectListItem()
                    {
                        Value = r.room_id.ToString(),
                        Text = r.TranslateName,
                        Selected = r.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetSpecialOffers()
        {
            return (from o in db.t_offer.ToList()
                    select new SelectListItem()
                    {
                        Value = o.offer_id.ToString(),
                        Text = o.TranslateName,
                        Selected = o.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetSpecialFeatures()
        {
            return (from f in db.t_feature.ToList()
                    select new SelectListItem()
                    {
                        Value = f.feature_id.ToString(),
                        Text = f.TranslateName,
                        Selected = f.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> Events()
        {
            return (from e in db.t_event_preference.ToList()
                    select new SelectListItem()
                    {
                        Value = e.eventpr_id.ToString(),
                        Text = e.TranslateName,
                        Selected = e.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetEvents()
        {
            return (from e in db.t_event_preference.ToList()
                    select new SelectListItem()
                    {
                        Value = e.eventpr_id.ToString(),
                        Text = e.TranslateName,
                        Selected = e.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        public List<SelectListItem> GetPayments()
        {
            return (from p in db.t_payment.ToList()
                    select new SelectListItem()
                    {
                        Value = p.payment_id.ToString(),
                        Text = p.TranslateName,
                        Selected = p.t_user.Where(u => u.user_id == COM.UserData.ID).Any()
                    }).ToList();
        }

        //public List<MyReferencesItem> AverageCheck { get; set; }

        public MyWorkingHours GetWorkingHours()
        {
            return (from w in db.t_work_hours
                    where w.user_id == COM.UserData.ID
                    select new MyWorkingHours()
                    {
                        BeginHours = w.whours_begin,
                        EndHours = w.whours_end,
                        ByTwentyFourHours = w.whours_tweтty_four.Value
                    }).FirstOrDefault();
        }

        public void EditRestaurantType(string[] restaurantTypes)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (restaurantTypes == null) restaurantTypes = new string[1];
                List<t_rest_type> deleteRestType = user.t_rest_type.Where(r => !restaurantTypes.Contains(r.type_id.ToString())).ToList();
                foreach (t_rest_type item in deleteRestType)
                    user.t_rest_type.Remove(item);

                db.SaveChanges();

                foreach (string item in restaurantTypes)
                {
                    long type_id = Convert.ToInt64(item);
                    if (!user.t_rest_type.Where(r => r.type_id == type_id).Any())
                        user.t_rest_type.Add(db.t_rest_type.Where(r => r.type_id == type_id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditCuisineType(string[] cuisineTypes)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (cuisineTypes == null) cuisineTypes = new string[1];
                List<t_cuisine> deleteCuisineType = user.t_cuisine.Where(a => !cuisineTypes.Contains(a.cuisine_id.ToString())).ToList();
                foreach (t_cuisine item in deleteCuisineType)
                    user.t_cuisine.Remove(item);

                db.SaveChanges();

                foreach (string item in cuisineTypes)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_cuisine.Any(a => a.cuisine_id == id))
                        user.t_cuisine.Add(db.t_cuisine.Where(a => a.cuisine_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditServices(string[] services)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (services == null) services = new string[1];
                List<t_service> deleteServices = user.t_service.Where(a => !services.Contains(a.service_id.ToString())).ToList();
                foreach (t_service item in deleteServices)
                    user.t_service.Remove(item);

                db.SaveChanges();

                foreach (string item in services)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_service.Any(a => a.service_id == id))
                        user.t_service.Add(db.t_service.Where(a => a.service_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditEntertainment(string[] entertainments)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (entertainments == null) entertainments = new string[1];
                List<t_entertaiment> deleteEntertaiment = user.t_entertaiment.Where(a => !entertainments.Contains(a.entertaiment_id.ToString())).ToList();
                foreach (t_entertaiment item in deleteEntertaiment)
                    user.t_entertaiment.Remove(item);

                db.SaveChanges();

                foreach (string item in entertainments)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_entertaiment.Any(a => a.entertaiment_id == id))
                        user.t_entertaiment.Add(db.t_entertaiment.Where(a => a.entertaiment_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditRooms(string[] rooms)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (rooms == null) rooms = new string[1];
                List<t_room> deleteRoom = user.t_room.Where(a => !rooms.Contains(a.room_id.ToString())).ToList();
                foreach (t_room item in deleteRoom)
                    user.t_room.Remove(item);

                db.SaveChanges();

                foreach (string item in rooms)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_room.Any(a => a.room_id == id))
                        user.t_room.Add(db.t_room.Where(a => a.room_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditSpecialOffers(string[] specialOffers)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (specialOffers == null) specialOffers = new string[1];
                List<t_offer> deleteOffer = user.t_offer.Where(a => !specialOffers.Contains(a.offer_id.ToString())).ToList();
                foreach (t_offer item in deleteOffer)
                    user.t_offer.Remove(item);

                db.SaveChanges();

                foreach (string item in specialOffers)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_offer.Any(a => a.offer_id == id))
                        user.t_offer.Add(db.t_offer.Where(a => a.offer_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditSpecialFeatures(string[] specialFeatures)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (specialFeatures == null) specialFeatures = new string[1];
                List<t_feature> deleteFeatures = user.t_feature.Where(a => !specialFeatures.Contains(a.feature_id.ToString())).ToList();
                foreach (t_feature item in deleteFeatures)
                    user.t_feature.Remove(item);

                db.SaveChanges();

                foreach (string item in specialFeatures)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_feature.Any(a => a.feature_id == id))
                        user.t_feature.Add(db.t_feature.Where(a => a.feature_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditPayments(string[] payments)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (payments == null) payments = new string[1];
                List<t_payment> deletePayment = user.t_payment.Where(a => !payments.Contains(a.payment_id.ToString())).ToList();
                foreach (t_payment item in deletePayment)
                    user.t_payment.Remove(item);

                db.SaveChanges();

                foreach (string item in payments)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_payment.Any(a => a.payment_id == id))
                        user.t_payment.Add(db.t_payment.Where(a => a.payment_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public void EditEvents(string[] events)
        {
            var user = db.t_user.Where(u => u.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                if (events == null) events = new string[1];
                List<t_event_preference> deletePayment = user.t_event_preference.Where(a => !events.Contains(a.eventpr_id.ToString())).ToList();
                foreach (t_event_preference item in deletePayment)
                    user.t_event_preference.Remove(item);

                db.SaveChanges();

                foreach (string item in events)
                {
                    long id = Convert.ToInt64(item);
                    if (!user.t_event_preference.Any(a => a.eventpr_id == id))
                        user.t_event_preference.Add(db.t_event_preference.Where(a => a.eventpr_id == id).FirstOrDefault());
                }
                db.SaveChanges();
            }
        }

        public List<SelectedPreferences> GetSelectedPreferences()
        {
            return ((

                    from r in GetRestaurantType()
                    where r.Selected
                    select new SelectedPreferences
                    {
                        ID = r.Value,
                        Name = r.Text,
                        TypePreferences = "RestaurantTypeSelected",
                        TargetPreferences = "RestaurantType"
                    }).Union(

                     from r in GetCuisineType()
                     where r.Selected
                     select new SelectedPreferences
                     {
                         ID = r.Value,
                         Name = r.Text,
                         TypePreferences = "CuisineTypeSelected",
                         TargetPreferences = "CuisineType"
                     }).Union(

                     from r in GetServices()
                     where r.Selected
                     select new SelectedPreferences
                     {
                         ID = r.Value,
                         Name = r.Text,
                         TypePreferences = "ServicesSelected",
                         TargetPreferences = "Services"
                     }).Union(

                    from r in GetEntertainment()
                    where r.Selected
                    select new SelectedPreferences
                    {
                        ID = r.Value,
                        Name = r.Text,
                        TypePreferences = "EntertainmentSelected",
                        TargetPreferences = "Entertainment"
                    }).Union(

                   from r in GetRooms()
                   where r.Selected
                   select new SelectedPreferences
                   {
                       ID = r.Value,
                       Name = r.Text,
                       TypePreferences = "RoomsSelected",
                       TargetPreferences = "Rooms"
                   }).Union(

                   from r in GetSpecialOffers()
                   where r.Selected
                   select new SelectedPreferences
                   {
                       ID = r.Value,
                       Name = r.Text,
                       TypePreferences = "SpecialOffersSelected",
                       TargetPreferences = "SpecialOffers"
                   }).Union(

                   from r in GetSpecialFeatures()
                   where r.Selected
                   select new SelectedPreferences
                   {
                       ID = r.Value,
                       Name = r.Text,
                       TypePreferences = "SpecialFeaturesSelected",
                       TargetPreferences = "SpecialFeatures"
                   }).Union(

                   from r in GetEvents()
                   where r.Selected
                   select new SelectedPreferences
                   {
                       ID = r.Value,
                       Name = r.Text,
                       TypePreferences = "EventsSelected",
                       TargetPreferences = "Events"
                   }).Union(

                   from r in GetPayments()
                   where r.Selected
                   select new SelectedPreferences
                   {
                       ID = r.Value,
                       Name = r.Text,
                       TypePreferences = "PaymentSelected",
                       TargetPreferences = "Payment"
                   }
                   )).OrderBy(o => o.Name).ToList();
        }

        public void DeleteRestaurantType(string restaurantType)
        {
            if (!string.IsNullOrEmpty(restaurantType))
            {
                long iRestType = Convert.ToInt64(restaurantType);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_rest_type.Remove(user.t_rest_type.FirstOrDefault(r => r.type_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeleteCuisineType(string cuisineType)
        {
            if (!string.IsNullOrEmpty(cuisineType))
            {
                long iRestType = Convert.ToInt64(cuisineType);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_cuisine.Remove(user.t_cuisine.FirstOrDefault(r => r.cuisine_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeleteServices(string services)
        {
            if (!string.IsNullOrEmpty(services))
            {
                long iRestType = Convert.ToInt64(services);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_service.Remove(user.t_service.FirstOrDefault(r => r.service_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeleteEntertainment(string entertainment)
        {
            if (!string.IsNullOrEmpty(entertainment))
            {
                long iRestType = Convert.ToInt64(entertainment);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_entertaiment.Remove(user.t_entertaiment.FirstOrDefault(r => r.entertaiment_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeleteRooms(string rooms)
        {
            if (!string.IsNullOrEmpty(rooms))
            {
                long iRestType = Convert.ToInt64(rooms);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_room.Remove(user.t_room.FirstOrDefault(r => r.room_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeleteSpecialOffers(string specialOffers)
        {
            if (!string.IsNullOrEmpty(specialOffers))
            {
                long iRestType = Convert.ToInt64(specialOffers);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_offer.Remove(user.t_offer.FirstOrDefault(r => r.offer_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeleteSpecialFeatures(string specialFeatures)
        {
            if (!string.IsNullOrEmpty(specialFeatures))
            {
                long iRestType = Convert.ToInt64(specialFeatures);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_feature.Remove(user.t_feature.FirstOrDefault(r => r.feature_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeletePayments(string payments)
        {
            if (!string.IsNullOrEmpty(payments))
            {
                long iRestType = Convert.ToInt64(payments);
                var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
                if (user != null)
                {
                    user.t_payment.Remove(user.t_payment.FirstOrDefault(r => r.payment_id == iRestType));
                    db.SaveChanges();
                }
            }
        }

        public void DeleteAllPreferences()
        {
            var user = db.t_user.Where(a => a.user_id == COM.UserData.ID).FirstOrDefault();
            if (user != null)
            {
                foreach (t_rest_type tresttype in user.t_rest_type.ToList())
                    user.t_rest_type.Remove(tresttype);

                foreach (t_payment tpayment in user.t_payment.ToList())
                    user.t_payment.Remove(tpayment);

                foreach (t_cuisine tcuisine in user.t_cuisine.ToList())
                    user.t_cuisine.Remove(tcuisine);

                foreach (t_service tservice in user.t_service.ToList())
                    user.t_service.Remove(tservice);

                foreach (t_entertaiment tentertaiment in user.t_entertaiment.ToList())
                    user.t_entertaiment.Remove(tentertaiment);

                foreach (t_room troom in user.t_room.ToList())
                    user.t_room.Remove(troom);

                foreach (t_offer toffer in user.t_offer.ToList())
                    user.t_offer.Remove(toffer);

                foreach (t_feature tfeature in user.t_feature.ToList())
                    user.t_feature.Remove(tfeature);

                foreach (t_payment tpayment in user.t_payment.ToList())
                    user.t_payment.Remove(tpayment);

                foreach (t_event_preference teventpreference in user.t_event_preference.ToList())
                    user.t_event_preference.Remove(teventpreference);

                db.SaveChanges();
            }
        }
    }
}