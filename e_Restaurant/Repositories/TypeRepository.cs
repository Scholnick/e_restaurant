﻿using e_Restaurant.Common;
using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace e_Restaurant.Repositories
{
    public class TypeRepository
    {
        private local db;
        public TypeRepository()
        {
            db = new local();
        }

        public List<TypeModel> GetTypeList(long RestaurantID)
        {
            string browserLang = Common.COM.BrowserData.Language;
            var types = (from t in db.t_types.Where(t => t.rest_id == RestaurantID && !t.type_id_parent.HasValue).ToList()
                         orderby t.type_id_parent, t.type_order
                         select new TypeModel
                         {
                             ID = t.type_id,
                             Name = HttpUtility.HtmlDecode(t.TranslateName),
                             NativeName = HttpUtility.HtmlDecode(t.NativeName),
                             Icon = ImageLink.Types.FullIconPath(t.t_restaurant.rest_ref, t.type_icon)
                         }).ToList();

            foreach (TypeModel type in types)
            {
                type.SubTypeList = (from t in db.t_types.Where(ts => ts.type_id_parent == type.ID &&
                                                                     ts.t_unit.Where(t => t.unit_active > 0).Any()
                                                               ).ToList()
                                    orderby t.type_order
                                    select new TypeModel
                                    {
                                        ID = t.type_id,
                                        Name = HttpUtility.HtmlDecode(t.TranslateName),
                                        NativeName = HttpUtility.HtmlDecode(t.NativeName)
                                    }).ToList();
            }

            return types.Where(t => t.SubTypeList.Any()).ToList();
        }
    }
}