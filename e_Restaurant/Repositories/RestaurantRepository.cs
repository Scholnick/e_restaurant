﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Restaurant.Common;
using e_Restaurant.Models;
using PagedList;
using PagedList.Mvc;

namespace e_Restaurant.Repositories
{
    public class RestaurantRepository
    {
        private local db;
        public RestaurantRepository()
        {
            db = new local();
        }

        private readonly TypeRepository typeRepo = new TypeRepository();
        private readonly UnitRepository unitRepo = new UnitRepository();
        private readonly ArticleRepository articleRepo = new ArticleRepository();
        private readonly RatingRepsitory ratingRepo = new RatingRepsitory();

        public RestaurantDetailsModel RestaurantDetails(string name)
        {
            string namerest = name.ToLower().Replace("_", " ");

            var rest = (from r in db.t_restaurant.Where(r => r.rest_name.ToLower() == namerest || r.rest_ref.ToLower() == name.ToLower()).ToList()
                        select new RestaurantDetailsModel()
                        {
                            ID = r.rest_id,
                            Name = r.rest_name,
                            Title = r.TitleTranslation,
                            Reference = r.rest_ref,
                            Description = r.FullDescription,
                            Phone = r.rest_phone,
                            Email = r.rest_email,
                            WebSite = r.rest_external_url,
                            Address = r.t_address != null ? r.t_address.Select(a => a.adr_address_1).FirstOrDefault() : "",
                            City = r.t_address != null ? r.t_address.Select(a=>a.t_city.city_name).FirstOrDefault(): "",
                            AddressAditional = r.t_address != null ? r.t_address.Select(a => a.adr_address_2).FirstOrDefault() : "",
                            Logo = r.LogoFullPath,
                            AditionalImage = r.AdditionalImageFullPath,
                            WorkingHours = r.rest_shedule,
                            SocialNetwork = r.t_rest_soc.ToDictionary(s => s.dict_code, s => s.url_soc),
                            Payment = r.t_rest_pay.Select(p => p.dict_code).ToArray(),
                            Langtitude = r.t_address != null ? r.t_address.Select(a => a.adr_lat).FirstOrDefault() : "",
                            LongTitude = r.t_address != null ? r.t_address.Select(a => a.adr_lng).FirstOrDefault() : ""
                        }).FirstOrDefault();

            if (rest != null)
            {
                rest.TypeList = typeRepo.GetTypeList(rest.ID);
                rest.UnitList = unitRepo.GetUnitList(rest.ID);
                rest.Rating = ratingRepo.RestaurantsRating(rest.ID);

                rest.AverageRating = ratingRepo.AverageScore(rest.ID);
                var yourRate = ratingRepo.RestaurantsOwnRating(rest.ID);
                if (yourRate != null)
                {
                    rest.RateFood = yourRate.Where(v => v.RatingCategory == (int)RatingCategory.Food).Select(v => v.Score).FirstOrDefault();
                    rest.RateService = yourRate.Where(v => v.RatingCategory == (int)RatingCategory.Service).Select(v => v.Score).FirstOrDefault();

                    rest.RateAmbience = yourRate.Where(v => v.RatingCategory == (int)RatingCategory.Ambience).Select(v => v.Score).FirstOrDefault();
                    rest.RateValue = yourRate.Where(v => v.RatingCategory == (int)RatingCategory.Value).Select(v => v.Score).FirstOrDefault();
                }
            }

            return rest;
        }

        public IEnumerable<RestaurantStartPageModel> GetListStartPage()
        {
            var restList = from r in db.t_restaurant.Where(r => r.t_unit.Any()).ToList()
                           select new RestaurantStartPageModel()
                           {
                               ID = r.rest_id,
                               Name = r.rest_name,
                               Description = r.FullDescription,
                               Ref = r.rest_ref,
                               Logo = r.LogoFullPath
                           };

            return from r in restList
                   join a in db.t_address.ToList() on r.ID equals a.rest_id into ag
                   from adress in ag.DefaultIfEmpty()
                   orderby r.Name
                   select new RestaurantStartPageModel
                   {
                       ID = r.ID,
                       Name = r.Name,
                       Description = r.Description,
                       Ref = r.Ref,
                       Logo = r.Logo,
                       Adress = adress != null ? adress.adr_address_1 : "",
                       ZipCode = adress != null ? adress.adr_zip_code : "",
                       CountryName = adress != null && adress.city_id.HasValue ? adress.t_city.t_country.TranslateName : "",
                       City = adress != null && adress.city_id.HasValue ? adress.t_city.TranslateName : "",
                       CityID = adress != null && adress.city_id.HasValue ? adress.city_id : (int?)null,
                       Langtitude = adress != null ? adress.adr_lat : "",
                       LongTitude = adress != null ? adress.adr_lng : ""
                   };
        }

        public IEnumerable<RestaurantStartPageModel> GetListByCity(string city)
        {
            return GetListStartPage().Where(l => l.City == city);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                }
                this.disposed = true;
            }
        }
    }
}