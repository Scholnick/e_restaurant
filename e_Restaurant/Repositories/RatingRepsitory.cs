﻿using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Restaurant.Common;

namespace e_Restaurant.Repositories
{
    public class RatingRepsitory
    {
        private local db;
        public RatingRepsitory()
        {
            db = new local();
        }

        public void Vote(RestaurantDetailsModel restaurant)
        {
            var userID = COM.UserData.ID;

            if (restaurant.ID > 0 && restaurant.RateAmbience > 0 &&
                restaurant.RateFood > 0 && restaurant.RateService > 0 && restaurant.RateValue > 0)
            {
                var myRateList = db.t_rate.Where(r => r.rest_id == restaurant.ID &&
                                                 r.user_id == userID).ToList();

                if (myRateList != null && myRateList.Count > 0)
                {
                    //present vote. Edit
                    /*Food = 1,
                     *Ambience = 2,
                     *Value = 3,
                     *Service = 4*/

                    var rateFood = myRateList.Where(r => r.rate_category == (int)RatingCategory.Food).FirstOrDefault();
                    if (rateFood != null)
                        rateFood.rate_score = restaurant.RateFood;

                    var rateAmbience = myRateList.Where(r => r.rate_category == (int)RatingCategory.Ambience).FirstOrDefault();
                    if (rateAmbience != null)
                        rateAmbience.rate_score = restaurant.RateAmbience;

                    var rateValue = myRateList.Where(r => r.rate_category == (int)RatingCategory.Value).FirstOrDefault();
                    if (rateValue != null)
                        rateValue.rate_score = restaurant.RateValue;

                    var rateService = myRateList.Where(r => r.rate_category == (int)RatingCategory.Service).FirstOrDefault();
                    if (rateService != null)
                        rateService.rate_score = restaurant.RateService;
                }
                else
                {
                    var tRateFood = new t_rate()
                        {
                            rest_id = restaurant.ID,
                            rate_category = (int)RatingCategory.Food,
                            rate_score = Convert.ToDecimal(restaurant.RateFood),
                            user_id = userID
                        };
                    db.t_rate.Add(tRateFood);

                    var tRateAmbience = new t_rate()
                    {
                        rest_id = restaurant.ID,
                        rate_category = (int)RatingCategory.Ambience,
                        rate_score = Convert.ToDecimal(restaurant.RateAmbience),
                        user_id = userID
                    };
                    db.t_rate.Add(tRateAmbience);

                    var tRateValue = new t_rate()
                    {
                        rest_id = restaurant.ID,
                        rate_category = (int)RatingCategory.Value,
                        rate_score = Convert.ToDecimal(restaurant.RateValue),
                        user_id = userID
                    };
                    db.t_rate.Add(tRateValue);

                    var tRateService = new t_rate()
                    {
                        rest_id = restaurant.ID,
                        rate_category = (int)RatingCategory.Service,
                        rate_score = Convert.ToDecimal(restaurant.RateService),
                        user_id = userID
                    };
                    db.t_rate.Add(tRateService);
                }
                db.SaveChanges();
            }
        }

        public string AverageScore(long restID)
        {
            if (db.t_rate.Where(r => r.rest_id == restID).Any())
                return Math.Round(db.t_rate.Where(r => r.rest_id == restID).Average(r => r.rate_score), 1).ToString();
            else
                return "";
        }

        public List<RatingAverageModel> RestaurantsRating(long restID)
        {
            return (from r in db.t_rate.Where(v => v.rest_id == restID).ToList()
                    group r by new { r.rest_id, r.rate_category } into gr
                    select new RatingAverageModel
                    {
                        RestID = gr.Key.rest_id,
                        RatingCategory = gr.Key.rate_category,
                        AverageRate = Math.Round(gr.Average(a => a.rate_score), 2)
                    }).ToList();
        }

        public List<MyRatingModel> RestaurantsOwnRating(long restID)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                var userID = COM.UserData.ID;
                return (from r in db.t_rate.Where(v => v.rest_id == restID && v.user_id == userID).ToList()
                        group r by new { r.rest_id, r.rate_category } into gr
                        select new MyRatingModel
                        {
                            RestID = gr.Key.rest_id,
                            RatingCategory = gr.Key.rate_category,
                            Score = Convert.ToInt32(gr.Average(a => a.rate_score))
                        }).ToList();
            }
            else
                return null;
        }
    }
}