﻿using e_Restaurant.Common;
using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace e_Restaurant
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static List<string> LanguageList = new List<string>();

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "Api/{controller}"
                );

            routes.MapHttpRoute(
                name: "DefaultApiID",
                routeTemplate: "Api/{controller}/{id}",
                defaults: null,
                constraints: new { id = @"^\d+$" }
                );

            routes.MapHttpRoute(
                name: "DefaultApiAction",
                routeTemplate: "Api/{controller}/{action}/{id}",
                defaults: null,
                constraints: new { id = @"^\d+$" }
            );*/

            routes.MapRoute("Restaurant", "Restaurant/{name}",
                           new { controller = "Restaurant", action = "Index", name = ".+" }
                           );

            routes.MapRoute("City", "City/{city}/{page}",
                            new { controller = "Restaurant", action = "City", city = ".+", page = UrlParameter.Optional }
                           );

            routes.MapRoute("MainSearch", "Search/{q}",
                            new { controller = "Search", action = "Index", q = UrlParameter.Optional }
                           );

            routes.MapRoute("RestaurantSearch", "Search/Restaurants/{searchstring}/{page}",
                           new { controller = "Search", action = "Restaurants", searchstring = UrlParameter.Optional, page = UrlParameter.Optional }
                           );

            routes.MapRoute("CitiesSearch", "Search/Cities/{searchstring}/{page}",
                            new { controller = "Search", action = "Cities", searchstring = ".+", page = UrlParameter.Optional }
                           );

            routes.MapRoute("ArticlesSearch", "Search/Articles/{searchstring}",
                            new { controller = "Search", action = "Articles", searchstring = ".+" }
                           );

            routes.MapRoute("Paging", "Restaurants/Page/{page}",
                            new { controller = "Home", action = "Index" },
                            new { page = "\\d+" }
                           );

            routes.MapRoute("Home", "Restaurants",
                            new { controller = "Home", action = "Index" }
                           );

            routes.MapRoute("PopularPaging", "Article/Popular/Page/{page}",
                            new { controller = "Article", action = "Popular" },
                            new { page = "\\d+" }
                           );

            routes.MapRoute("ArticleList", "Articles",
                            new { controller = "Article", action = "Index" }
                           );

            routes.MapRoute("ArticlePopular", "Article/Popular",
                           new { controller = "Article", action = "Popular" }
                           );

            routes.MapRoute("NewPaging", "Article/New/Page/{page}",
                            new { controller = "Article", action = "New" },
                            new { page = "\\d+" }
                            );
            routes.MapRoute("ArticleNews", "Article/New",
                           new { controller = "Article", action = "New" }
                           );


            routes.MapRoute("ArticleEdit", "Article/Edit/{id}/{articlename}",
                           new { controller = "Article", action = "Edit", id = UrlParameter.Optional, articlename = UrlParameter.Optional }
                           );

            routes.MapRoute("ArticlePrewiev", "Article/Activate/{id}/{articlename}",
                           new { controller = "Article", action = "Activate", id = @"^\d+$", articlename = UrlParameter.Optional }
                           );
            routes.MapRoute("ArticleDeactivate", "Article/Deactivate/{id}/{articlename}",
               new { controller = "Article", action = "Deactivate", id = @"^\d+$", articlename = UrlParameter.Optional }
               );

            routes.MapRoute("ArticleDetails", "Article/{id}/{articlename}",
                           new { controller = "Article", action = "Details", id = @"^\d+$", articlename = UrlParameter.Optional }
                           );

            routes.MapRoute("ArticleDelete", "Article/Delete/{id}/{articlename}",
                           new { controller = "Article", action = "Delete", id = @"^\d+$", articlename = UrlParameter.Optional }
                           );
            routes.MapRoute("ArticleTranslateDetails", "Article/Translate/{id}/{articlename}",
                           new { controller = "Article", action = "Translate", id = @"^\d+$", articlename = UrlParameter.Optional }
                           );

            routes.MapRoute("ArticleTranslateEdit", "Article/Translate/Edit/{id}/{langid}",
               new { controller = "Article", action = "EditArtTranslate", id = UrlParameter.Optional, langid = UrlParameter.Optional }
               );

            routes.MapRoute("Default", "{controller}/{action}/{id}",
                            new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                           );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // Use LocalDB for Entity Framework by default
            Database.DefaultConnectionFactory = new SqlConnectionFactory(@"Data Source=(localdb)\v11.0; Integrated Security=True; MultipleActiveResultSets=True");

            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredLocalizedAttribute), typeof(RequiredAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RegularExpressionLocalizedAttribute), typeof(RegularExpressionAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(StringLengthLocalizedAttribute), typeof(StringLengthAttributeAdapter));

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            COM cm = new COM();
            cm.CreateFilesTranslation();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null &&
                HttpContext.Current.User.Identity.IsAuthenticated &&
                HttpContext.Current.User.Identity is FormsIdentity)
            {
                HttpContext.Current.User = new UserPrincipal(HttpContext.Current.User.Identity is UserIdentity ?
                    HttpContext.Current.User.Identity as UserIdentity : new UserIdentity(((FormsIdentity)HttpContext.Current.User.Identity).Ticket));
            }
        }

    }
}