﻿using e_Restaurant.Common;
using e_Restaurant.Models;
using e_Restaurant.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;

namespace e_Restaurant.Controllers
{
    public class ArticleController : Controller
    {
        private ArticleRepository articleRepo = new ArticleRepository();
        private LanguageRepository langRepo = new LanguageRepository();

        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            var article = articleRepo.GetList().Where(a => a.UserID == COM.UserData.ID).OrderByDescending(a => a.data_created);
            return View(article);
        }

        public ActionResult Popular(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            var article = new PopularPage()
            {
                Popular = articleRepo.ArticlePopular().ToPagedList(pageNumber, pageSize),
                New = articleRepo.ArticleNew().Take(5)
            };

            return View(article);
        }

        public ActionResult New(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            var article = new NewrPage()
             {
                 Popular = articleRepo.ArticlePopular().Take(5),
                 New = articleRepo.ArticleNew().ToPagedList(pageNumber, pageSize)
             };
            return View(article);
        }

        [Authorize(Roles = "Admin, User")]
        public ActionResult Activate(int id, string articlename)
        {
            var articleModel = new ArticleActivate()
            {
                Article = articleRepo.FindById(id),
                Activate = false
            };
            return View(articleModel);

        }

        [HttpPost]
        public ActionResult Activate(ArticleActivate article, int id)
        {
            articleRepo.Activate(id);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin, User")]
        public ActionResult Deactivate(int id, string articlename)
        {
            var articleModel = new ArticleActivate()
            {
                Article = articleRepo.FindById(id),
                Activate = false
            };
            return View(articleModel);

        }

        [HttpPost]
        public ActionResult Deactivate(ArticleActivate article, int id)
        {
            articleRepo.Deactivate(id);
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id = 0, string articlename = "")
        {
            var articleDetails = new DetailsModel()
            {
                ArticleDetails = articleRepo.Details(id)
            };
            if (articleDetails.ArticleDetails != null)
            {
                if (articlename != StringHelpers.ToSeoUrl(articleDetails.ArticleDetails.title))
                    return RedirectToActionPermanent("Details", new { id = id, articlename = StringHelpers.ToSeoUrl(articleDetails.ArticleDetails.title) });
                else
                {
                    articleRepo.UpdateCountArticle(id);
                    articleDetails.ArticleListPopular = articleRepo.ArticlePopular();
                    return View(articleDetails);
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult EditArtTranslate(int id, int langId)
        {
            var articleTranslate = articleRepo.GetOneArticleTranslate(id, langId);
            return View(articleTranslate);
        }

        [HttpPost]
        public ActionResult EditArtTranslate(ArticleEditTranslate article)
        {
            if (ModelState.IsValid)
            {
                articleRepo.Update(article);
            }
            else
            {
                return View(article);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Translate(int id, string articlename)
        {
            var articleTranslate = new ArticleTranslateModel()
            {
                Article = articleRepo.Details(id),
                Languages = langRepo.GetList()
            };
            return View(articleTranslate);
        }

        [HttpPost]
        public ActionResult Translate(ArticleTranslateModel article, int id)
        {
            articleRepo.AddTranslate(article, id);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin, User, Restaurant")]
        public ActionResult Edit(int id = 0, string articlename = "")
        {
            if (id != 0)
                return View(articleRepo.FindById(id));
            else
                return View(new ArticleModel() { Languages = langRepo.GetListForEdit() });
        }

        // Publish
        [HttpParamAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Publish(HttpPostedFileBase[] logo, ArticleModel article)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int? ID = (int?)null;
                    foreach (var file in logo)
                    {
                        if (article.ID != 0)
                        {
                            ID = article.ID;
                            articleRepo.Update(article, file);
                            articleRepo.Activate((int)ID);
                        }
                        else
                        {
                            ID = articleRepo.Create(article, file);
                            articleRepo.Activate((int)ID);
                        }
                        return RedirectToAction("Details", new { id = ID });
                    }
                }
                else
                {
                    if (article.Languages == null || article.Languages.Count() == 0)
                        article.Languages = langRepo.GetListForEdit();
                    return View(article);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return View(article);
        }

        // SaveDraft
        [HttpParamAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveDraft(HttpPostedFileBase[] logo, ArticleModel article)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int? ID = (int?)null;
                    foreach (var file in logo)
                    {
                        if (article.ID != 0)
                        {
                            ID = article.ID;
                            articleRepo.Update(article, file);
                            articleRepo.Deactivate((int)ID);
                        }
                        else
                        {
                            ID = articleRepo.Create(article, file);
                            articleRepo.Deactivate((int)ID);
                        }
                        return RedirectToAction("Details", new { id = ID });
                    }
                }
                else
                {
                    if (article.Languages == null || article.Languages.Count() == 0)
                        article.Languages = langRepo.GetListForEdit();
                    return View(article);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return View(article);
        }

        [Authorize(Roles = "Admin, User,Restaurant")]
        public ActionResult Delete(int id, string articlename)
        {
            var article = articleRepo.Details(id);
            return View(article);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id, string articlename)
        {
            articleRepo.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            articleRepo.Dispose();
            base.Dispose(disposing);
        }
    }
}