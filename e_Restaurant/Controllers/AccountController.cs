﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using e_Restaurant.Models;
using e_Restaurant.Repositories;
using e_Restaurant.Common;

namespace e_Restaurant.Controllers
{
    public class AccountController : Controller
    {
        private UserRepository userRepo = new UserRepository();
        private CountryRepository countryRepo = new CountryRepository();
        private MyPreferencesRepository preferencesRepo = new MyPreferencesRepository();
        private ErrorModel errorModel = new ErrorModel();
        private CityRepository cityRepo = new CityRepository();

        // GET: /Account/LogOn
        public ActionResult LogOn()
        {      
            Session["basicUrl"] = System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
            return View();
        }

        // POST: /Account/LogOn
        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (userRepo.ValidateUser(model.UserEmail, model.Password))
                {
                    DateTime expiredate = DateTime.Now.AddMinutes(15);

                    if (model.RememberMe)
                        expiredate = DateTime.Now.AddHours(24);

                    var userData = userRepo.GetIdentityData(model.UserEmail);
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                        1, model.UserEmail, DateTime.Now, expiredate, false,
                        String.Format("{0}|{1}|{2}|{3}|{4}|{5}", userData.UserID.ToString(), userData.FirstName, userData.LastName, userData.FullUserName, userData.Email, userData.Role));

                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
                    if (model.RememberMe)
                        cookie.Expires = ticket.Expiration;

                    Response.Cookies.Add(cookie);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 &&
                        returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") &&
                        !returnUrl.StartsWith("/\\"))
                        return Redirect(returnUrl);
                    else
                        //return RedirectToAction("Index", "Home");
                        return Redirect(Session["basicUrl"].ToString());

                }
                else
                {
                    ModelState.AddModelError("", COM.GetTranslation("LABEL_LOGIN_PASSW_INCORRECT"));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        // GET: /Account/Register
        [HttpGet]
        public ActionResult Register()
        {
            var registerModel = new RegisterModel();
            registerModel.CountryList = countryRepo.GetList();
            var countryID = Convert.ToInt32(countryRepo.GetList().FirstOrDefault().Value);
            registerModel.CityList = cityRepo.GetList(countryID);
            return View(registerModel);
        }

        // POST: /Account/Register
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                userRepo.CreateUser(model);

                COM.SendEmail(model.Email, "E-RestaurantNFC. Activate your account",
                    EmailTemplate.Registration
                    .Replace("$User_name$", model.FirstName)
                    .Replace("$Email$", model.Email)
                    .Replace("$password$", model.Password)
                    .Replace("$URL$", COM.Url.SiteUrl.TrimEnd('/') + Url.Action("Activate", "Account", new { @id = model.ActivateGuid })));

                return RedirectToAction("RegisterSuccessful");
            }
            else
            {
                var registerModel = new RegisterModel();
                registerModel.CountryList = countryRepo.GetList();
                var countryID = Convert.ToInt32(countryRepo.GetList().FirstOrDefault().Value);
                registerModel.CityList = cityRepo.GetList(countryID);
                return View(registerModel);
            }
        }

        public ActionResult RegisterSuccessful()
        {
            return View();
        }

        //GET: /Account/Activate/guid
        public ActionResult Activate(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Register", "Account");
            else
                if (COM.BrowserData.QueryString.ToString().IndexOf("email") == -1)
                    return View(userRepo.GetUserForActivate(id));
                else
                    return View(userRepo.GetUserForActivate(id, true));

        }

        // check email for unique
        public ActionResult ValidateEmail(string email)
        {
            return Json(userRepo.IsValidEmail(email), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Profile()
        {
            var profile = userRepo.GetProfileData();
            profile.CountryList = countryRepo.GetList();
            profile.CityList = cityRepo.GetList(profile.Country);
            profile.SexList = userRepo.GetSexList();
            profile.ListPhoneCode = countryRepo.GetCountryPhoneCodes();
            profile.ChangePassword = new ChangePasswordModel();
            profile.ChangeEmail = new ChangeEmailModel()
                    {
                        FirstName = profile.FirstName,
                        Guid = profile.ActivateGuid,
                        OldEmail = profile.Email
                    };
            profile.DeleteAccount = new DeleteAccountModel();
            return View(profile);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Profile(ProfileModel model)
        {
            if (ModelState.IsValid)
                userRepo.SaveProfileData(model);
            var profile = userRepo.GetProfileData();
            profile.CountryList = countryRepo.GetList();
            profile.CityList = cityRepo.GetList(profile.Country);
            profile.SexList = userRepo.GetSexList();
            profile.ListPhoneCode = countryRepo.GetCountryPhoneCodes();
            profile.ChangePassword = new ChangePasswordModel();
            profile.ChangeEmail = new ChangeEmailModel()
                                    {
                                        FirstName = profile.FirstName,
                                        Guid = profile.ActivateGuid,
                                        OldEmail = profile.Email
                                    };
            profile.DeleteAccount = new DeleteAccountModel();
            return View(profile);
        }

        // GET: /Account/ChangePassword
        [Authorize]
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return PartialView(new ChangePasswordModel());
        }

        // POST: /Account/ChangePassword
        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = userRepo.ChangePassword(model);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    errorModel.Message = COM.GetTranslation("LABEL_PASSWORD_SUCCESS_CHANGED");
                    errorModel.IsError = false;
                    return Json(ViewToString.RenderViewToString(this, "~/Views/Shared/_Error.cshtml", errorModel), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ModelState.AddModelError("", COM.GetTranslation("LABEL_INVALID_CHANGE_PASSWORD"));
                    errorModel.Message = COM.GetTranslation("LABEL_INVALID_CHANGE_PASSWORD");
                    errorModel.IsError = true;
                    return Json(ViewToString.RenderViewToString(this, "~/Views/Shared/_Error.cshtml", errorModel), JsonRequestBehavior.AllowGet);
                }

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult ChangeEmail()
        {
            return PartialView(new ChangeEmailModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeEmail(ChangeEmailModel model)
        {
            if (ModelState.IsValid)
            {
                bool changeEmailSucceeded;
                try
                {
                    changeEmailSucceeded = userRepo.ChangeEmail(model);
                }
                catch (Exception)
                { changeEmailSucceeded = false; }
                if (changeEmailSucceeded)
                {
                    COM.SendEmail(model.Email, "E-RestaurantNFC. Confirm change email",
                    "Dear <b>" + model.FirstName + "</b>!<br/>" +
                    "If you want to change email address <br/>" +
                    "Click <a href=" + COM.Url.SiteUrl + "/Account/Activate/" + model.Guid + "?email>Confirm</a>  to verify the new email address!.<br/>" +
                    "<br/><br/>" +
                    "Regards, E-RestaurantNFC."
                    );
                    errorModel.Message = string.Format(COM.GetTranslation("LABEL_CHECK_NEW_EMAIL"), model.Email);
                    errorModel.IsError = false;
                    return Json(ViewToString.RenderViewToString(this, "~/Views/Shared/_Error.cshtml", errorModel), JsonRequestBehavior.AllowGet);
                }
                else
                    ModelState.AddModelError("", "Error");
            }
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Delete()
        {
            return PartialView(new DeleteAccountModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(DeleteAccountModel deleteModel)
        {
            if (ModelState.IsValid)
            {
                bool isDeleted;
                try
                {
                    isDeleted = userRepo.DeleteAccount(deleteModel);
                }
                catch (Exception ex)
                {
                    isDeleted = false;
                }
                if (isDeleted)
                    return View();
                else
                    ModelState.AddModelError("", "Error");
            }
            return PartialView(new DeleteAccountModel());
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPaswordModel forgotModel)
        {
            if (ModelState.IsValid)
            {
                if (!userRepo.IsValidEmail(forgotModel.Email))
                {
                    string password = UserRepository.GeneratePassword(6);
                    string userName = "";
                    bool IsChanged = userRepo.ResetPassword(forgotModel, password, out userName);
                    if (IsChanged)
                    {
                        COM.SendEmail(forgotModel.Email, "E-RestaurantNFC. Reset password",
                                     "Dear <b>" + userName + "</b>!<br/>" +
                                     "As requested, here is your new password: " + password + "<br/>" +
                                     "<br/><br/>" +
                                     "Regards, The E-RestaurantNFC team"
                                     );
                        errorModel.Message = COM.GetTranslation("LABEL_SENT_NEW_PASSWORD");
                        errorModel.IsError = false;
                        return Json(ViewToString.RenderViewToString(this, "~/Views/Shared/_Error.cshtml", errorModel), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ModelState.AddModelError("", COM.GetTranslation("LABEL_ACCOUNT_NOT_EXIST"));
                        errorModel.Message = COM.GetTranslation("LABEL_ACCOUNT_NOT_EXIST");
                        errorModel.IsError = true;
                        return Json(ViewToString.RenderViewToString(this, "~/Views/Shared/_Error.cshtml", errorModel), JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("", COM.GetTranslation("LABEL_ACCOUNT_NOT_EXIST"));
                    errorModel.Message = COM.GetTranslation("LABEL_ACCOUNT_NOT_EXIST");
                    errorModel.IsError = true;
                    return Json(ViewToString.RenderViewToString(this, "~/Views/Shared/_Error.cshtml", errorModel), JsonRequestBehavior.AllowGet);
                }
            }
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult MyPreferences()
        {
            return View(new MyPreferencesModel()
                            {
                                RestaurantType = new MyRestaurantType(),
                                CuisineType = new MyCuisineType(),
                                Services = new MyService(),
                                Entertainment = new MyEntertainment(),
                                Rooms = new MyRooms(),
                                SpecialOffers = new MySpecialOffers(),
                                SpecialFeatures = new MySpecialFeature(),
                                Payments = new MyPayment(),
                                Events = new MyEvent(),
                                SelectedPreferences = new MyPreferencesSelectedModel(),
                                WorkingHours = new MyWorkingHours()
                            });
        }

        [Authorize]
        [HttpPost]
        public ActionResult MyPreferences(MyPreferencesModel myPreferencesModel)
        {
            preferencesRepo.EditRestaurantType(myPreferencesModel.SelectedRestaurantType);
            preferencesRepo.EditCuisineType(myPreferencesModel.SelectedCuisineType);
            preferencesRepo.EditServices(myPreferencesModel.SelectedServices);
            preferencesRepo.EditEntertainment(myPreferencesModel.SelectedEntertainment);
            preferencesRepo.EditRooms(myPreferencesModel.SelectedRooms);
            preferencesRepo.EditSpecialOffers(myPreferencesModel.SelectedSpecialOffers);
            preferencesRepo.EditSpecialFeatures(myPreferencesModel.SelectedSpecialFeatures);
            preferencesRepo.EditPayments(myPreferencesModel.SelectedPayments);
            preferencesRepo.EditEvents(myPreferencesModel.SelectedEvents);
            var selectedPreferences = new MyPreferencesSelectedModel();
            return Json(
                ViewToString.RenderViewToString(this, "~/Views/Account/MySelectedPreferences.cshtml", selectedPreferences)
                , JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpGet]
        public ActionResult MySelectedPreferences()
        {
            return View(new MyPreferencesSelectedModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult MySelectedPreferences(MyPreferencesSelectedModel myPreferencesModel)
        {
            if (string.IsNullOrEmpty(myPreferencesModel.IsCleanAll))
            {
                preferencesRepo.DeleteRestaurantType(myPreferencesModel.RestaurantTypeSelected);
                preferencesRepo.DeleteCuisineType(myPreferencesModel.CuisineTypeSelected);
                preferencesRepo.DeleteServices(myPreferencesModel.ServicesSelected);
                preferencesRepo.DeleteEntertainment(myPreferencesModel.EntertainmentSelected);
                preferencesRepo.DeleteRooms(myPreferencesModel.RoomsSelected);
                preferencesRepo.DeleteSpecialOffers(myPreferencesModel.SpecialOffersSelected);
                preferencesRepo.DeleteSpecialFeatures(myPreferencesModel.SpecialFeaturesSelected);
                preferencesRepo.DeletePayments(myPreferencesModel.PaymentSelected);
            }
            else
                preferencesRepo.DeleteAllPreferences();

            myPreferencesModel.SelectedPreferences = preferencesRepo.GetSelectedPreferences();
            return Json(ViewToString.RenderViewToString(this, "~/Views/Account/MySelectedPreferences.cshtml", myPreferencesModel), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCity(int id, string term)
        {
            return Json(cityRepo.GetCityList(id, term), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCountryList(int id)
        {
            var countryID = Convert.ToInt64(id);
            return Json(cityRepo.GetList(countryID), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Terms()
        {
            return View();
        }
               
    }
}
