﻿using e_Restaurant.Models;
using e_Restaurant.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;

namespace e_Restaurant.Controllers
{
    public class SearchController : Controller
    {
        private readonly SearchRepository searchRepo = new SearchRepository();
        private readonly RestaurantRepository restaurantRepo = new RestaurantRepository();
        private readonly ArticleRepository articleRepo = new ArticleRepository();

        [HttpGet]
        public ActionResult Index(string q)
        {
            return PartialView(searchRepo.Search(q));
        }

        public ActionResult Cities(string searchstring, int? page)
        {
            int pageSize = 6;
            int pageNumber = (page ?? 1);

            var searchCityModel = new RestaurantSearchCityModel();
            searchCityModel.Result = searchRepo.RestaurantByCities(searchstring).ToPagedList(pageNumber, pageSize);
            searchCityModel.ArticlePopular = articleRepo.ArticlePopular();
            return View(searchCityModel);
        }

        public ActionResult Restaurants(string searchstring, int? page)
        {
            if (string.IsNullOrEmpty(searchstring))
                return RedirectToAction("Index", "Home");
            else
            {
                int pageSize = 6;
                int pageNumber = (page ?? 1);
                var searchRestaurant = new RestaurantSearchCityModel();
                searchRestaurant.Result = searchRepo.RestaurantName(searchstring).ToPagedList(pageNumber, pageSize);
                searchRestaurant.ArticlePopular = articleRepo.ArticlePopular();
                return View(searchRestaurant);
            }
        }

        public ActionResult Articles(string searchstring)
        {            
            var searchArticleModel = new ArticleSearchModel();
            searchArticleModel.Result = searchRepo.Articles(searchstring);
            searchArticleModel.ArticlePopular = articleRepo.ArticlePopular();
            return View(searchArticleModel);
        }
    }
}
