﻿using e_Restaurant.Models;
using e_Restaurant.Repositories;
using PagedList;
using PagedList.Mvc;
using System;
using e_Restaurant.Common;
using System.Web.Mvc;

namespace e_Restaurant.Controllers
{
    public class HomeController : Controller
    {
        private RestaurantRepository restaurantsRepo = new RestaurantRepository();
        private ArticleRepository articleRepo = new ArticleRepository();

        public ActionResult Index(int? page)
        {            
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var listStartPage = new StartPageModel()
            {
                RestaurantList = restaurantsRepo.GetListStartPage().ToPagedList(pageNumber, pageSize),
                ArticleDet = articleRepo.ArticleNew()
            };
            return View(listStartPage);
        }

        public ActionResult About()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            var message = new ContactModel();

            return View(message);
           
        }
        [HttpPost]
        public ActionResult ContactUs(ContactModel message)
        {
            if (!ModelState.IsValid)
                return View(message);

            COM.SendEmail(message.Email, message.Subject, message.Message);
            return View("ContactSuccess");
        }
    }
}
