﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using NLog;

namespace e_Restaurant.Controllers
{
    public class ThumbnailController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void Index(string ImagePath, int width, int height)
        {
            string imagePath = Server.MapPath("~/" + ImagePath);
            if (System.IO.File.Exists(imagePath))
                new WebImage(Server.MapPath("~/" + ImagePath)).Resize(width, height, true).Write();
            else
                Logger.Error(string.Format("Missing file {0}", imagePath));
        }
    }
}
