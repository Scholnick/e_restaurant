﻿using e_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_Restaurant.Repositories;
using e_Restaurant.Common;
using PagedList.Mvc;
using PagedList;

namespace e_Restaurant.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly RestaurantRepository restaurantRepo = new RestaurantRepository();
        private readonly UnitRepository unitRepo = new UnitRepository();
        private readonly ArticleRepository articleRepo = new ArticleRepository();
        private readonly RatingRepsitory ratingRepo = new RatingRepsitory();

        [HttpGet]
        public ActionResult Index(string name)
        {
            var restaurant = restaurantRepo.RestaurantDetails(name);
            if (restaurant != null)                            
                return View(restaurant);            
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Index(RestaurantDetailsModel restaurant)
        {
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                ratingRepo.Vote(restaurant);
            }
            return RedirectToAction("Index", new { name = restaurant.Reference });
        }

        [HttpGet]
        public ActionResult GetUnitList(int id)
        {
            return Json(
                ViewToString.RenderViewToString(this, "~/Views/Restaurant/_Menu.cshtml", unitRepo.GetUnitListByID(id))
                , JsonRequestBehavior.AllowGet);
        }

        public ActionResult City(string city, int? page)
        {
            if (string.IsNullOrEmpty(city))
                return RedirectToAction("Index", "Home");
            else
            {
                int pageSize = 6;
                int pageNumber = (page ?? 1);
                var searchCityModel = new RestaurantSearchCityModel();
                searchCityModel.Result = restaurantRepo.GetListByCity(city).ToPagedList(pageNumber, pageSize);
                searchCityModel.ArticlePopular = articleRepo.ArticlePopular();

                return View(searchCityModel);
            }
        }
    }
}
