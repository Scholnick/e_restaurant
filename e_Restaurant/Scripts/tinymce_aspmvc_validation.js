$.validator.setDefaults({
        ignore: []
    });

    $(document).ready(function () {
        close_wait();

        var tinyMceOptions = {
            mode: "exact",
            elements: "textAreaForTooltipText",
            theme: "advanced",
            content_css : "/Scripts/themes/advanced/skins/default/custom_content.css",
            language: "pl",
            entity_encoding : "raw",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,asciimath,autoresize", 
            theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
            font_size_style_values : "10px,12px,13px,14px,16px,18px,20px",
            theme_advanced_runtime_fontsize: '12px',
            theme_advanced_fonts : "Arial=arial,helvetica,sans-serif;Cambria = cambria, georgia, serif;Courier New=courier new,courier,monospace;Helvetica = Helvetica, Arial, sans-serif;Times New Roman = Times, Baskerville, Georgia, serif;Tahoma = Tahoma, Verdana, Segoe, sans-serif;Verdana = Verdana, Geneva, sans-serif",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,|,bullist,numlist,|,outdent,indent,|,link,unlink,image,asciimathcharmap,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            file_browser_callback : "filebrowser",
            setup: validationTinyMceSetup
        }
        
        
        tinyMCE.init(tinyMceOptions);

        var isArticle = '@Model.IsArticleSubject';

        if (isArticle === 'True') {
            tinyMCE.EditorManager.execCommand('mceAddControl',true, 'textAreaForArtitleTitleTooltipText');
        }

        $('#formulaSubjectEditForm').submit(function () {
            tinyMCE.triggerSave();
            if ($('#formulaSubjectEditForm').valid()) {
                open_wait();
            } 
        });

        $('#article-input').click(function () {
            if( $('#article-input').is(':checked') ) {
                $('#tooltip').text('Nazwa czasopisma');
                $('#with-article').show();
                tinyMCE.EditorManager.execCommand('mceAddControl',true, 'textAreaForArtitleTitleTooltipText');
            } else {
                $('#tooltip').text('Tekst dymku');
                $('#with-article').hide();
            }

        });

    });

    function validationTinyMceSetup(editor) {
        var $textarea = $('#' + editor.editorId);

        // method to use to save editor contents to backend input field (TinyMCE hides real input and syncs it up
        // with values on form submit) -- we need to sync up the hidden input fields and call the valid()
        // method from jQuery unobtrusive validation if it is present
        function save(editor) {
            if (editor.isDirty) {
                editor.save();
                var $input = $('#' + editor.editorId);
                if (typeof $input.valid === 'function')
                    $input.valid();
            }
        }

        // Save tinyMCE contents to input field on key/up down (efficiently so IE-old friendly)
        var typingTimerDown, typingTimerUp;
        var triggerDownSaveInterval = 1000;     // time in ms
        var triggerUpSaveInterval = 500;        // time in ms

        editor.onKeyDown.add(function (editor) {
            clearTimeout(typingTimerDown);
            typingTimerDown = setTimeout(function () { save(editor) }, triggerDownSaveInterval);
        });

        editor.onKeyUp.add(function () {
            clearTimeout(typingTimerUp);
            typingTimerUp = setTimeout(function () { save(editor) }, triggerUpSaveInterval);
        });


        // Save tinyMCE contents to input field on deactivate (when focus leaves editor)
        // this is via TAB
        editor.onKeyDown.add(function (editor, event) {
            if (event.keyCode === 9)
                save(editor);
        });

        // this is when focus goes from one editor to another (however the last one never
        // triggers -- need to enter another TinyMCE for event to trigger!)
        editor.onDeactivate.add(function (editor) {
            save(editor);
        });
    }