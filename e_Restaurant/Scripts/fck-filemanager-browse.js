function browseToFileManagerServer(target, url) {
    var width = 600;
    var height = 400;

    var iLeft = (window.screen.width - width) / 2;
    var iTop = (window.screen.height - height) / 2;

    var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes,scrollbars=yes,center=1";
    sOptions += ",width=" + width;
    sOptions += ",height=" + height;
    sOptions += ",left=" + iLeft;
    sOptions += ",top=" + iTop;

    //get url 
    var localUrl;
    var connector;
    var rootPath = window.location.protocol + "//" + window.location.host + "/";
    localUrl = rootPath + "Scripts/fckeditor/editor/filemanager/browser/default/browser.html";
    connector = rootPath + "Scripts/fckeditor/editor/filemanager/connectors/asp/connector.asp";

    //if (window.location.hostname == "localhost") {
    //    localUrl = rootPath + "Scripts/fckeditor/editor/filemanager/browser/default/browser.html";
    //    connector = rootPath + "Scripts/fckeditor/editor/filemanager/connectors/asp/connector.asp";
    //}

    //else {
    //    localUrl = rootPath + "test/Scripts/fckeditor/editor/filemanager/browser/default/browser.html";
    //    connector = rootPath + "test/Scripts/fckeditor/editor/filemanager/connectors/asp/connector.asp";
    //}
    var fullUrl = localUrl + "?Type=Image&Connector=" + connector;

    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
    var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

    if (isFirefox) {
        var result = window.showModalDialog(fullUrl, "File/Image Management Server", sOptions);

        if (result) {
            target.value = result;
            console.log(target.value);
        }
    }
    else {
        //console.log('not mozila');
        //}
        //if (isOpera || isChrome) {
        var win = window.open(fullUrl, "File/Image Management Server", sOptions);
        var timer = setInterval(function () {
            if (win.closed) {
                clearInterval(timer);
                var returnValue = win.returnValue;
                //console.log("target =" + returnValue);
            }
            target.value = win.returnValue;
        }, 2500);
        //console.log("target.value =" + target.value);
    }
};

